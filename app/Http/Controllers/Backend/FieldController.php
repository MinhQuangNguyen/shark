<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Category\StoreCategoryRequest;
use App\Http\Requests\Backend\Category\EditCategoryRequest;
use App\Http\Requests\Backend\Category\UpdateCategoryRequest;
use App\Http\Requests\Backend\Category\DeleteCategoryRequest;
use App\Models\Category;
use App\Services\CategoryService;
use App\Http\Controllers\Controller;

/**
 * Class FieldController.
 */
class FieldController extends Controller
{
    /**
     * @var CategoryService
     */
    protected $categoryService;
    const PATH_BANNER_FIELD = '/images/field/banner';

    /**
     * FieldController constructor.
     *
     * @param  CategoryService  $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.field.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.field.create');
    }

    /**
     * @param  StoreCategoryRequest  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StoreCategoryRequest $request)
    {
        $data = $request->only('name', 'name_en', 'image', 'description', 'description_en', 'slug', 'slug_en', 'sort', 'active');
        $banner = $request->file('banner');
        if($banner) {
            $bannerName = time().'.'.$banner->getClientOriginalExtension();
            $destinationPathB = public_path(self::PATH_BANNER_FIELD);
            $banner->move($destinationPathB, $bannerName);
            $data['banner'] = self::PATH_BANNER_FIELD.'/'.$bannerName;
        }
        $data['type'] = Category::TYPE_PROJECT;
        $this->categoryService->store($data, 'field');

        return redirect()->route('admin.field.index')->withFlashSuccess(__('The field was successfully created.'));
    }

    /**
     * @param  EditCategoryRequest  $request
     * @param  Category  $field
     *
     * @return mixed
     */
    public function edit(EditCategoryRequest $request, Category $field)
    {
        return view('backend.field.edit')->withField($field);
    }

    /**
     * @param  UpdateCategoryRequest  $request
     * @param  Category  $field
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateCategoryRequest $request, Category $field)
    {
        $data = $request->validated();
        $banner = $request->file('banner');
        if($banner) {
            $bannerName = time().'.'.$banner->getClientOriginalExtension();
            $destinationPathB = public_path(self::PATH_BANNER_FIELD);
            $banner->move($destinationPathB, $bannerName);
            $data['banner'] = self::PATH_BANNER_FIELD.'/'.$bannerName;
        }
        $data['type'] = Category::TYPE_PROJECT;
        $this->categoryService->update($field, $data, 'field');

        return redirect()->route('admin.field.index')->withFlashSuccess(__('The field was successfully updated.'));
    }

    /**
     * @param  DeleteCategoryRequest  $request
     * @param  Category  $field
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(DeleteCategoryRequest $request, Category $field)
    {
        $this->categoryService->destroy($field, 'field');

        return redirect()->route('admin.field.index')->withFlashSuccess(__('The field was successfully deleted.'));
    }
}
