<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class ImageController.
 */
class ImageController extends Controller
{

    public function storeMedia(Request $request)
    {
        $path = storage_path('tmp/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $files = $request->file('file');
        $arrFile = array();
        foreach($files as $file) {
            $name = uniqid(). '_'.trim($file->getClientOriginalName());
            $file->move($path, $name);
            array_push($arrFile, [
                'name'          => $name,
                'original_name' => $file->getClientOriginalName(),
            ]);
        }

        return response()->json($arrFile);
    }

    public function deleteMedia(Request $request) {
        $path = storage_path('tmp/uploads');
        $filename = $request->name;
        $file_path = $path . '/' . $filename;

        if (file_exists($file_path)) {
            unlink($file_path);
        }

        return response()->json(['message' => 'File successfully delete'], 200);
    }
}
