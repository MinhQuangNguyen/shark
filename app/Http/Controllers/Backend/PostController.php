<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Post\StorePostRequest;
use App\Http\Requests\Backend\Post\EditPostRequest;
use App\Http\Requests\Backend\Post\UpdatePostRequest;
use App\Http\Requests\Backend\Post\DeletePostRequest;
use App\Models\Post;
use App\Services\PostService;
use App\Services\CategoryService;
use App\Http\Controllers\Controller;

/**
 * Class PostController.
 */
class PostController extends Controller
{
    /**
     * @var PostService
     * @var CategoryService
     */
    protected $postService;
    protected $categoryService;
    const PATH_IMAGE = '/images/post';
    const PATH_BANNER = '/images/post/banner';

    /**
     * PostController constructor.
     *
     * @param  PostService  $postService
     * @param  CategoryService $categoryService
     */
    public function __construct(PostService $postService, CategoryService $categoryService)
    {
        $this->postService = $postService;
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.post.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.post.create')
            ->withCategories($this->categoryService->getNewLibraryCategories());
    }

    /**
     * @param  StorePostRequest  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StorePostRequest $request)
    {
        $data = $request->only('title', 'title_en', 'description_short', 'description_short_en', 'slug', 'slug_en', 'type', 'link', 'sort', 'active', 'category_id', 'is_highlight', 'content', 'content_en');
        $image = $request->file('image');
        if($image) {
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path(self::PATH_IMAGE);
            $image->move($destinationPath, $imageName);
            $data['image'] = self::PATH_IMAGE.'/'.$imageName;
        }

        $banner = $request->file('banner');
        if($banner) {
            $imageBanner = time().'.'.$banner->getClientOriginalExtension();
            $destinationPathB = public_path(self::PATH_BANNER);
            $banner->move($destinationPathB, $imageBanner);
            $data['banner'] = self::PATH_BANNER.'/'.$imageBanner;
        }

        $this->postService->store($data);

        return redirect()->route('admin.post.index')->withFlashSuccess(__('The post was successfully created.'));
    }

    /**
     * @param  EditPostRequest  $request
     * @param  Post  $post
     *
     * @return mixed
     */
    public function edit(EditPostRequest $request, Post $post)
    {
        return view('backend.post.edit')
            ->withCategories($this->categoryService->getNewLibraryCategories())
            ->withPost($post);
    }

    /**
     * @param  UpdatePostRequest  $request
     * @param  Post  $post
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $data = $request->validated();
        $image = $request->file('image');
        if($image) {
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path(self::PATH_IMAGE);
            $image->move($destinationPath, $imageName);
            $data['image'] = self::PATH_IMAGE.'/'.$imageName;
        }

        $banner = $request->file('banner');
        if($banner) {
            $imageBanner = time().'.'.$banner->getClientOriginalExtension();
            $destinationPathB = public_path(self::PATH_BANNER);
            $banner->move($destinationPathB, $imageBanner);
            $data['banner'] = self::PATH_BANNER.'/'.$imageBanner;
        }

        $this->postService->update($post, $data);

        return redirect()->route('admin.post.index')->withFlashSuccess(__('The post was successfully updated.'));
    }

    /**
     * @param  DeletePostRequest  $request
     * @param  Post  $post
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(DeletePostRequest $request, Post $post)
    {
        $this->postService->destroy($post);

        return redirect()->route('admin.post.index')->withFlashSuccess(__('The post was successfully deleted.'));
    }
}
