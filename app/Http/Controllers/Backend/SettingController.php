<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Setting\EditSettingRequest;
use App\Http\Requests\Backend\Setting\UpdateSettingRequest;
use App\Http\Requests\Backend\Setting\DeleteSettingRequest;
use App\Models\Setting;
use App\Services\SettingService;
use App\Http\Controllers\Controller;

/**
 * Class SettingController.
 */
class SettingController extends Controller
{
    /**
     * @var SettingService
     */
    protected $settingService;

    /**
     * SettingController constructor.
     *
     * @param  SettingService  $settingService
     */
    public function __construct(SettingService $settingService)
    {
        $this->settingService = $settingService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.setting.index');
    }

    /**
     * @param  EditSettingRequest  $request
     * @param  Setting  $setting
     *
     * @return mixed
     */
    public function edit(EditSettingRequest $request, Setting $setting)
    {
        return view('backend.setting.edit')->withSetting($setting);
    }

    /**
     * @param  UpdateSettingRequest  $request
     * @param  Setting  $setting
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateSettingRequest $request, Setting $setting)
    {
        $data = $request->validated();
        $this->settingService->update($setting, $data);

        return redirect()->route('admin.setting.index')->withFlashSuccess(__('The setting was successfully updated.'));
    }

    /**
     * @param  DeleteSettingRequest  $request
     * @param  Setting  $setting
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(DeleteSettingRequest $request, Setting $setting)
    {
        $this->settingService->destroy($setting);

        return redirect()->route('admin.setting.index')->withFlashSuccess(__('The setting was successfully deleted.'));
    }
}
