<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\MenuService;
use App\Services\SettingService;


/**
 * Class AboutController.
 */
class AboutController extends Controller
{
    public function __construct(MenuService $menuService, SettingService $settingService)
    {
        $this->menuService = $menuService;
        $this->settingService = $settingService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($section){
        $subMenus = $this->menuService->getByKey("introduce");
        $title = "";
        $description = "";

        foreach($subMenus as $subMenu){
            if($subMenu->linkContains($section)){
                $subMenu->isActive = true;
                $title = $subMenu->name;
                $description = strip_tags($subMenu->content);
            }
        }

        $setting = $this->settingService->getSettingByKey('about');
        $about = ['name' => '', 'description' => ''];
        if($setting) $about = [
            'name' => $setting->name,
            'description' => $setting->value
        ];

        return view('frontend.about.index', [
            "subMenus" => $subMenus,
            "title" => $title,
            "description" => $description,
            "about" => $about
        ]);
    }

    public function vision()
    {
        return view('frontend.about.vision');
    }

    public function strategy()
    {
        return view('frontend.about.strategy');
    }

    public function mission()
    {
        return view('frontend.about.mission');
    }

    public function coreValues()
    {
        return view('frontend.about.coreValues');
    }

    public function brand()
    {
        return view('frontend.about.brand');
    }

    public function organizationalChart()
    {
        return view('frontend.about.organizationalChart');
    }
}
