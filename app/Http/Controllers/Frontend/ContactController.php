<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\ContactService;
use Illuminate\Http\Request;

/**
 * Class ContactController.
 */
class ContactController extends Controller
{
    /**
     * @var ContactService
     */
    protected $contactService;

    /**
     * ContactController constructor.
     *
     * @param  ContactService  $contactService
     */
    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.contact.index');
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $data = $request->only('name', 'email', 'title', 'content');
        $this->contactService->store($data);
        $locale = session()->get('locale') ?? env('APP_LANGUAGE');
        $pageName = $locale === "vi" ? 'lien-he' : 'contact';
        return redirect()->route('frontend.'.$pageName.'.index');
    }
}
