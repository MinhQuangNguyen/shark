<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\RouteService;


/**
 * Class DynamicRouteController.
 */
class DynamicRouteController extends Controller
{
    public function __construct(RouteService $routeService)
    {
        $this->routeService = $routeService;
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $url)
    {
        $route = $this->routeService->getByUrl($url);
        return \App::call('App\Http\Controllers\Frontend\\'.$route["view"]);
    }
}
