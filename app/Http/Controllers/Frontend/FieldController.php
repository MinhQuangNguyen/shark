<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\SettingService;
use App\Models\Category;
use Illuminate\Http\Request;

/**
 * Class FieldController.
 */
class FieldController extends Controller
{
    /**
     * ProjectController constructor.
     *
     * @param  CategoryService  $categoryService
     * @param  SettingService  $settingService
     */
    public function __construct(CategoryService $categoryService, SettingService $settingService)
    {
        $this->categoryService = $categoryService;
        $this->settingService = $settingService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $slug)
    {
        $fields = $this->categoryService->getByType([Category::TYPE_PROJECT]);
        $field = $this->categoryService->getCategoryBySlug($slug);
        $setting = $this->settingService->getSettingByKey('future');
        $future = ['name' => '', 'description' => ''];
        if($setting) $future = [
            'name' => $setting->name,
            'description' => $setting->value
        ];
        // return $fields;
        return view('frontend.field.index', [
            'fields' => $fields,
            'future' => $future,
            'field' => $field
        ]);
    }
}
