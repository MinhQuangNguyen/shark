<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\ProjectService;
use Illuminate\Http\Request;

/**
 * Class ProjectController.
 */
class ProjectController extends Controller
{
    /**
     * ProjectController constructor.
     *
     * @param  ProjectService  $projectService
     * @param  CategoryService  $categoryService
     */
    public function __construct(ProjectService $projectService, CategoryService $categoryService)
    {
        $this->projectService = $projectService;
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $slug = null)
    {
        $categories = $this->categoryService->getProjectCategories();
        $category_id = null;

        if($slug) {
            $category = $this->categoryService->getCategoryBySlug($slug);
            $category_id = $category->id;
        }

        $keyword = $request->get('keyword');
        $projects = $this->projectService->getProjectByCategory($category_id, $keyword);
        $slides = $this->projectService->getProjectOutstanding($category_id, 3);
        $res = [];
        foreach($slides as $slide) {
            array_push($res, [
                'title' => $slide->name,
                'image' => $slide->banner,
            ]);
        }
        return view('frontend.project.index', [
            'categories' => $categories,
            'slides' => json_decode(json_encode($res)),
            'projects' => $projects
        ]);
    }

    public function detail(Request $request, $categorySlug, $slug) {
        $project = $this->projectService->getProjectBySlug($slug);
        $slides = [
            [
                'title' => $project->name,
                'image' => $project->banner,
            ]
        ];
        // return $project;
        return view('frontend.project.detail', [
            'slides' => json_decode(json_encode($slides)),
            'project' => $project
        ]);
    }

    public function overview()
    {
        return view('frontend.project.includes.overview');
    }

    public function location()
    {
        return view('frontend.project.location');
    }

    public function utilities()
    {
        return view('frontend.project.utilities');
    }

    public function libraryImage()
    {
        return view('frontend.project.libraryImage');
    }

    public function field()
    {
        return view('frontend.project.field');
    }
}
