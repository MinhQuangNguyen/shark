<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\RecruitmentService;
use App\Services\CareerService;
use App\Services\AddressService;
use Illuminate\Http\Request;

/**
 * Class RecruitmentController.
 */
class RecruitmentController extends Controller
{
    /**
     * RecruitmentController constructor.
     *
     * @param  RecruitmentService  $recruitmentService
     * @param  CategoryService  $categoryService
     * @param  CareerService  $careerService
     * @param  AddressService  $addressService
     */
    public function __construct(RecruitmentService $recruitmentService, CategoryService $categoryService, CareerService $careerService, AddressService $addressService)
    {
        $this->recruitmentService = $recruitmentService;
        $this->categoryService = $categoryService;
        $this->careerService = $careerService;
        $this->addressService = $addressService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $slides = $this->categoryService->getSlideByKey('recruitment');
        $core_values = $this->categoryService->getSlideByKey('recruitment-value');
        $images = $this->categoryService->getSlideByKey('recruitment-images');
        $recuitments = $this->recruitmentService->getRecruitmentTypical();

        return view('frontend.recruitment.index', [
            'slides' => $slides,
            'core_values' => $core_values,
            'images' => $images,
            'recuitments' => $recuitments
        ]);
    }

    public function list(Request $request, $slug = null)
    {
        $careers = $this->careerService->getCareers();
        $addresses = $this->addressService->getAddresses();
        $keyword = $request->get('keyword');
        $career_id = $request->get('career_id');
        $address_id = $request->get('address_id');
        $wage = $request->get('wage');
        $wages = [
            [ 'id' => 1, 'value' => [5000000, 10000000], 'label' => '5.000.000 - 10.000.000' ],
            [ 'id' => 2, 'value' => [10000000, 15000000], 'label' => '10.000.000 - 15.000.000' ],
            [ 'id' => 3, 'value' => [15000000, 20000000], 'label' => '15.000.000 - 20.000.000' ]
        ];
        $rs = current(array_filter($wages, function($item) use ($wage) {
            return $item['id'] == $wage;
        }));
        if($rs) $wage = $rs['value'];
        else $wage = null;
        $recuitments = $this->recruitmentService->getRecruitmentByCategory($address_id, $career_id, $wage, $keyword);

        return view('frontend.recruitment.list', [
            'careers' => $careers,
            'addresses' => $addresses,
            'recuitments' => $recuitments,
            'wages' => $wages
        ]);
    }

    public function detail(Request $request, $slug)
    {
        $recruitment = $this->recruitmentService->getRecruitmentBySlug($slug);
        return view('frontend.recruitment.detail', [
            'recruitment' => $recruitment
        ]);
    }
}
