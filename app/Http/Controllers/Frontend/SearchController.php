<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\SearchService;
use Illuminate\Http\Request;

/**
 * Class SearchController.
 */
class SearchController extends Controller
{
    /**
     * ProjectController constructor.
     *
     * @param  SearchService  $searchService
     */
    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('k');
        $page = $request->get('page');
        $projects = $this->searchService->getResultByCondition($keyword, $page);

        return view('frontend.search.result', [
            'results' => $projects
        ]);
    }
}
