<?php

namespace App\Http\Livewire\Backend;

use App\Models\Project;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Traits\HtmlComponents;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class ProjectsTable.
 */
class ProjectsTable extends TableComponent
{
    use HtmlComponents;

    /**
     * @var string
     */
    public $sortField = 'name';

    /**
     * @var array
     */
    protected $options = [
        'bootstrap.container' => false,
        'bootstrap.classes.table' => 'table table-striped',
    ];

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Project::query();
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Name'), 'name')
                ->searchable(function ($builder, $term) {
                    return $builder->whereTranslationLike('name', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('projects.*')->join('project_translations as t', function ($query) use($locale) {
                        $query->on('projects.id', '=', 't.project_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.name', $direction);
                })
                ->format(function (Project $model) {
                    return view('backend.project.includes.name', ['project' => $model]);
                }),
            Column::make(__('Address'), 'address')
                ->searchable(function ($builder, $term) {
                    return $builder->orWhereTranslationLike('address', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('projects.*')->join('project_translations as t', function ($query) use($locale) {
                        $query->on('projects.id', '=', 't.project_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.address', $direction);
                })
                ->format(function (Project $model) {
                    return view('backend.project.includes.address', ['project' => $model]);
                }),
            Column::make(__('Slug'), 'slug')
                ->searchable(function ($builder, $term) {
                    return $builder->orWhereTranslationLike('slug', '%'.$term.'%');
                })
                ->sortable(function ($builder, $direction) {
                    $locale = session()->get('locale') ?? env('APP_LANGUAGE');
                    return $builder->select('projects.*')->join('project_translations as t', function ($query) use($locale) {
                        $query->on('projects.id', '=', 't.project_id')
                            ->where('t.locale', '=', $locale);
                    })->orderBy('t.slug', $direction);
                })
                ->format(function (Project $model) {
                    return view('backend.project.includes.slug', ['project' => $model]);
                }),
            Column::make(__('Field'), 'category_id')
                ->format(function (Project $model) {
                    $field = Category::find($model->category_id);
                    return view('backend.project.includes.field', ['field' => $field]);
                }),
            Column::make(__('Highlight'), 'is_highlight')
                ->sortable()
                ->format(function (Project $model) {
                    return view('backend.project.includes.highlight', ['project' => $model]);
                }),
            Column::make(__('Actions'))
                ->format(function (Project $model) {
                    return view('backend.project.includes.actions', ['project' => $model]);
                }),
        ];
    }
}
