<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    // protected $fillable = array('*');
    protected $guarded = [];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }
}
