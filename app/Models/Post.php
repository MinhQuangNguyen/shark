<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Post extends Model implements TranslatableContract
{
    use Translatable;

    public const TYPE_NEW_IMAGE = 0;
    public const TYPE_VIDEO = 1;
    public const ACTIVE = 1;
    public const INACTIVE = 0;
    public $translatedAttributes = ['title', 'slug', 'description_short', 'content'];

    protected $fillable = ['category_id', 'image', 'banner', 'is_highlight', 'type', 'link', 'sort', 'created_at'];
    protected $guarded = [];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}
