<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecruitmentTranslation extends Model
{
    protected $fillable = ['title', 'slug', 'description', 'content', 'rank', 'form', 'location', 'contact', 'note'];
    public $timestamps = false;

    protected $guarded = [];
    protected $casts = ['form' => 'array'];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }
}
