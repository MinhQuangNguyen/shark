<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SlideTranslation extends Model
{
    protected $fillable = ['title', 'content', 'link'];
    public $timestamps = false;

    protected $guarded = [];

    public function getOptionsAttribute($value)
    {
        return unserialize($value);
    }
}
