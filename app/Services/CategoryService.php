<?php

namespace App\Services;

use App\Events\Category\CategoryCreated;
use App\Events\Category\CategoryDeleted;
use App\Events\Category\CategoryUpdated;
use App\Models\Category;
use App\Models\Slide;
use App\Models\Post;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class CategoryService.
 */
class CategoryService extends BaseService
{
    /**
     * CategoryService constructor.
     *
     * @param  Category  $category
     */
    public function __construct(Category $category, Slide $slide, Post $post)
    {
        $this->model = $category;
        $this->slide = $slide;
        $this->post = $post;
    }

    /**
     * @return mixed
     */
    public function getAllCategories()
    {
        return $this->model::query()->where('active', 1)->whereIn('type', [Category::TYPE_NEW, Category::TYPE_PROJECT, Category::TYPE_LIBRARY])->get();
    }

    /**
     * @return mixed
     */
    public function getProjectCategories()
    {
        return $this->model::query()->where('active', 1)->where('type', Category::TYPE_PROJECT)->get();
    }

    /**
     * @return mixed
     */
    public function getNewCategories()
    {
        return $this->model::query()->where('active', 1)->whereIn('type', [Category::TYPE_NEW, Category::TYPE_LIBRARY])->get();
    }

    /**
     * @return mixed
     */
    public function getNewLibraryCategories()
    {
        return $this->model::query()->where('active', 1)->whereIn('type', [Category::TYPE_NEW, Category::TYPE_LIBRARY])->get();
    }

    /**
     * @param  array  $data
     *
     * @return Category
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = [], string $type = 'category'): Category
    {
        DB::beginTransaction();
        try {
            $dataT = [
                'type' => isset($data['type']) ? $data['type'] : null,
                'image' => isset($data['image']) ? $data['image'] : null,
                'banner' => isset($data['banner']) ? $data['banner'] : null,
                'sort' => isset($data['sort']) ? $data['sort'] : null,
                'active' => isset($data['active']) ? $data['active'] : null,
                'vi' => [
                    'name' => isset($data['name']) ? $data['name'] : null,
                    'description' => isset($data['description']) ? $data['description'] : null,
                    'slug' => isset($data['slug']) ? $data['slug'] : null
                ],
                'en' => [
                    'name' => isset($data['name_en']) ? $data['name_en'] : null,
                    'description' => isset($data['description_en']) ? $data['description_en'] : null,
                    'slug' => isset($data['slug_en']) ? $data['slug_en'] : null
                ]
            ];
            $category = $this->model::create($dataT);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem creating the '.$type.'.'));
        }

        event(new CategoryCreated($category));

        DB::commit();

        return $category;
    }

    /**
     * @param  Category  $category
     * @param  array  $data
     *
     * @return Category
     * @throws GeneralException
     * @throws \Throwable
     */
    public function update(Category $category, array $data = [], string $type = 'category'): Category
    {
        DB::beginTransaction();
        try {
            $locale = session()->get('locale') ?? env('APP_LANGUAGE');
            $dataT = [
                'type' => isset($data['type']) ? $data['type'] : $category->type,
                'image' => isset($data['image']) ? $data['image'] : $category->image,
                'banner' => isset($data['banner']) ? $data['banner'] : $category->banner,
                'sort' => isset($data['sort']) ? $data['sort'] : $category->sort,
                'active' => isset($data['active']) ? $data['active'] : $category->active,
                'key' => isset($data['key']) ? $data['key'] : $category->key,
                $locale => [
                    'name' => isset($data['name']) ? $data['name'] : null,
                    'description' => isset($data['description']) ? $data['description'] : null,
                    'slug' => isset($data['slug']) ? $data['slug'] : null,
                ]
            ];
            $category->update($dataT);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating the '.$type.'.'));
        }

        event(new CategoryUpdated($category));

        DB::commit();

        return $category;
    }

    /**
     * @param  Category  $category
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Category $category, string $type = 'category'): bool
    {
        if ($category->menus()->count()) {
            throw new GeneralException(__('You can not delete a category with associated menus.'));
        }

        if ($this->deleteById($category->id)) {
            event(new CategoryDeleted($category));

            return true;
        }

        throw new GeneralException(__('There was a problem deleting the '.$type.'.'));
    }


    public function getByType($type = [])
    {
        return $this->model->query()
        ->whereIn('type', $type)
        ->where('active', 1)
        ->orderBy('sort', 'asc')
        ->get();
    }

    public function getPostByCategoryType($type) {
        $category = $this->model->query()->where('type', $type)->where('active', 1)->first();
        $result = [];
        if(isset($category)) {
            $result = $this->post->query()
                ->where('category_id', $category->id)
                ->orderBy('sort', 'asc')
                ->get();
        }
        return $result;
    }

    public function getSlideByKey($key)
    {
        $category = $this->model->query()->where('key', $key)->where('active', 1)->first();
        $result = [];
        if(isset($category)) {
            $result = $this->slide->query()
                ->where('category_id', $category->id)
                ->orderBy('sort', 'asc')
                ->get();
        }
        return $result;
    }

    public function getCategoryBySlug($slug) {
        $category = $this->model->query()->where('active', 1)->whereTranslationLike('slug', $slug)->first();
        return $category;
    }
}
