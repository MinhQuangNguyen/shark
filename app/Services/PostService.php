<?php

namespace App\Services;

use App\Events\Post\PostCreated;
use App\Events\Post\PostDeleted;
use App\Events\Post\PostUpdated;
use App\Models\Post;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class PostService.
 */
class PostService extends BaseService
{
    /**
     * PostService constructor.
     *
     * @param  Post  $post
     */
    public function __construct(Post $post)
    {
        $this->model = $post;
    }

    /**
     * @param  array  $data
     *
     * @return Post
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): Post
    {
        DB::beginTransaction();
        try {
            $dataT = [
                'type' => isset($data['type']) ? $data['type'] : null,
                'image' => isset($data['image']) ? $data['image'] : null,
                'banner' => isset($data['banner']) ? $data['banner'] : null,
                'is_highlight' => isset($data['is_highlight']) ? $data['is_highlight'] : 0,
                'sort' => isset($data['sort']) ? $data['sort'] : null,
                'active' => isset($data['active']) ? $data['active'] : null,
                'link' => isset($data['link']) ? $data['link'] : null,
                'category_id' => isset($data['category_id']) ? $data['category_id'] : null,
                'vi' => [
                    'title' => isset($data['title']) ? $data['title'] : null,
                    'description_short' => isset($data['description_short']) ? $data['description_short'] : null,
                    'slug' => isset($data['slug']) ? $data['slug'] : null,
                    'content' => isset($data['content']) ? $data['content'] : null
                ],
                'en' => [
                    'title' => isset($data['title_en']) ? $data['title_en'] : null,
                    'description_short' => isset($data['description_short_en']) ? $data['description_short_en'] : null,
                    'slug' => isset($data['slug_en']) ? $data['slug_en'] : null,
                    'content' => isset($data['content_en']) ? $data['content_en'] : null
                ]
            ];
            $post = $this->model::create($dataT);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem creating the post.'));
        }

        event(new PostCreated($post));

        DB::commit();

        return $post;
    }

    /**
     * @param  Post  $post
     * @param  array  $data
     *
     * @return Post
     * @throws GeneralException
     * @throws \Throwable
     */
    public function update(Post $post, array $data = []): Post
    {
        DB::beginTransaction();
        try {
            $locale = session()->get('locale') ?? env('APP_LANGUAGE');
            $dataT = [
                'type' => isset($data['type']) ? $data['type'] : $post->type,
                'image' => isset($data['image']) ? $data['image'] : $post->image,
                'banner' => isset($data['banner']) ? $data['banner'] : $post->image,
                'is_highlight' => isset($data['is_highlight']) ? $data['is_highlight'] : 0,
                'sort' => isset($data['sort']) ? $data['sort'] : $post->sort,
                'active' => isset($data['active']) ? $data['active'] : $post->active,
                'link' => isset($data['link']) ? $data['link'] : $post->link,
                'category_id' => isset($data['category_id']) ? $data['category_id'] : $post->category_id,
                $locale => [
                    'title' => isset($data['title']) ? $data['title'] : $post->title,
                    'description_short' => isset($data['description_short']) ? $data['description_short'] : $post->description_short,
                    'slug' => isset($data['slug']) ? $data['slug'] : $post->slug,
                    'content' => isset($data['content']) ? $data['content'] : $post->content
                ]
            ];
            $post->update($dataT);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem updating the post.'));
        }

        event(new PostUpdated($post));

        DB::commit();

        return $post;
    }

    /**
     * @param  Post  $post
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Post $post): bool
    {
        if ($this->deleteById($post->id)) {
            event(new PostDeleted($post));

            return true;
        }

        throw new GeneralException(__('There was a problem deleting the post.'));
    }

    //lấy tất cả bài viết theo category
    public function getPostByCategory($category_id = null, $keyword = null) {
        $query = $this->model->query()->orderBy('created_at', 'desc');
        if($category_id) $query = $query->where('category_id', $category_id);
        if($keyword) $query = $query->where(function($builder) use($keyword){
            $builder->orWhereTranslationLike('title',"%$keyword%");
            $builder->orWhereTranslationLike('description_short','like', "%$keyword%");
        });

        return $query->paginate(6);
    }

    public function getPostBySlug($slug) {
        $post = $this->model->query()->whereTranslationLike('slug', $slug)->first();
        return $post;
    }

    public function getPostRelate($category_id, $post_id, $type) {
        $query = $this->model->query();
        return $query
        ->where('id', '!=', $post_id)
        ->where('type', $type)
        ->where('category_id', $category_id)
        ->orderBy('sort', 'asc')->limit(6)->get();
    }

    public function getPostHighLight($category_id = null, $limit = 3) {
        $query = $this->model->query();
        if($category_id) $query = $query->where('category_id', $category_id);
        return $query
            ->where('active', 1)
            ->orderBy('created_at', 'desc')->limit($limit)->get();
    }
}
