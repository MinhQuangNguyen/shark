<?php

namespace App\Services;

use App\Models\Project;
use App\Models\Post;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Illuminate\Pagination\LengthAwarePaginator;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class SearchService.
 */
class SearchService extends BaseService
{
    /**
     * SearchService constructor.
     *
     * @param  Project  $project
     * @param  Post  $post
     */
    public function __construct(Project $project, Post $post)
    {
        $this->project = $project;
        $this->post = $post;
    }

    //lấy tất cả dự án theo category
    public function getResultByCondition($keyword = null, $page = 1, $limit = 6) {
        $locale = session()->get('locale') ?? env('APP_LANGUAGE');
        $query = $this->project->query()->join('project_translations as pj', function ($query) use($locale) {
            $query->on('projects.id', '=', 'pj.project_id')
                ->where('pj.locale', '=', $locale);
        })->select('name', 'description', 'category_id', 'slug');

        if($keyword) $query = $query->where(function($builder) use($keyword){
            $builder->orWhereTranslationLike('name', "%$keyword%");
            $builder->orWhereTranslationLike('description', 'like', "%$keyword%");
        });

        $post = $this->post->query()->join('post_translations as t', function ($query) use($locale) {
            $query->on('posts.id', '=', 't.post_id')
                ->where('t.locale', '=', $locale);
        })->select('t.title as name', 't.description_short as description', 'category_id', 'slug');
        if($keyword) $post = $post->where(function($builder) use($keyword){
            $builder->orWhereTranslationLike('title', "%$keyword%");
            $builder->orWhereTranslationLike('description_short', 'like', "%$keyword%");
        });

        $data = $query->get()->concat($post->get());
        $slice = $data->slice($limit * ($page - 1), $limit);
        return new LengthAwarePaginator($slice, count($data), $limit, $page);
    }
}
