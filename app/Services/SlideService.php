<?php

namespace App\Services;

use App\Events\Slide\SlideCreated;
use App\Models\Slide;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class SlideService.
 */
class SlideService extends BaseService
{
    /**
     * SlideService constructor.
     *
     * @param  Slide  $slide
     */
    public function __construct(Slide $slide)
    {
        $this->model = $slide;
    }

    /**
     * @return mixed
     */
    public function getSlides($category_id)
    {
        $query = $this->model::where('category_id', $category_id);
        return $query->orderBy('sort', 'asc')->get();
    }

    /**
     * @param  array  $data
     *
     * @return Slide
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = [], $category_id): Slide
    {
        DB::beginTransaction();
        try {
            $dataT = [
                'category_id' => $category_id,
                'image' => isset($data['image']) ? $data['image'] : null,
                'sort' => isset($data['sort']) ? $data['sort'] : 0,
                'vi' => [
                    'title' => isset($data['title']) ? $data['title'] : null,
                    'content' => isset($data['content']) ? $data['content'] : null,
                    'link' => isset($data['link']) ? $data['link'] : null
                ],
                'en' => [
                    'title' => isset($data['title_en']) ? $data['title_en'] : null,
                    'content' => isset($data['content_en']) ? $data['content_en'] : null,
                    'link' => isset($data['link_en']) ? $data['link_en'] : null
                ]
            ];
            $slide = $this->model::create($dataT);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem creating the slide.'));
        }

        event(new SlideCreated($slide));

        DB::commit();

        return $slide;
    }
}
