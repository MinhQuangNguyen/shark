<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyRecruitmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recruitments', function (Blueprint $table) {
            $table->unsignedBigInteger('degree_id')->after('address_id')->nullable();
            $table->foreign('degree_id')->references('id')->on('degrees');
            $table->unsignedBigInteger('department_id')->after('degree_id')->nullable();
            $table->foreign('department_id')->references('id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recruitments', function (Blueprint $table) {
            $table->dropForeign('recruitments_degree_id_foreign');
            $table->dropColumn('degree_id');
            $table->dropForeign('recruitments_department_id_foreign');
            $table->dropColumn('department_id');
        });
    }
}
