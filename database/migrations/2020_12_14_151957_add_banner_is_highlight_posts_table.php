<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBannerIsHighlightPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->text('banner')->after('image')->nullable();
            $table->tinyInteger('is_highlight')->after('category_id')->default(0)->comment('0: Không nổi bật, 1: nổi bật');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('banner');
            $table->dropColumn('is_highlight');
        });
    }
}
