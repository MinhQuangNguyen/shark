<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use App\Models\MenuTranslation;
use Database\Seeders\Traits\DisableForeignKeys;
use Database\Seeders\Traits\TruncateTable;

class MenuSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        $this->truncate('menus');
        $this->truncate('menu_translations');

        if (app()->environment(['local', 'testing'])) {
            Menu::insert([
                [
                    'parent_id' => null,
                    'category_id' => null,
                    'sort' => 1,
                    'key' => 'home',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => null,
                    'category_id' => null,
                    'sort' => 2,
                    'key' => 'introduce',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 2,
                    'category_id' => null,
                    'sort' => 1,
                    'key' => 'introduce',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 2,
                    'category_id' => null,
                    'sort' => 2,
                    'key' => 'introduce',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 2,
                    'category_id' => null,
                    'sort' => 3,
                    'key' => 'introduce',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 2,
                    'category_id' => null,
                    'sort' => 4,
                    'key' => 'introduce',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 2,
                    'category_id' => null,
                    'sort' => 5,
                    'key' => 'introduce',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 2,
                    'category_id' => null,
                    'sort' => 6,
                    'key' => 'introduce',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => null,
                    'category_id' => null,
                    'sort' => 4,
                    'key' => 'new',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 9,
                    'category_id' => 1,
                    'sort' => 1,
                    'key' => 'new',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 9,
                    'category_id' => 2,
                    'sort' => 2,
                    'key' => 'new',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 9,
                    'category_id' => null,
                    'sort' => 3,
                    'key' => 'new',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => null,
                    'category_id' => null,
                    'sort' => 3,
                    'key' => 'project',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 13,
                    'category_id' => null,
                    'sort' => 1,
                    'key' => 'project',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 13,
                    'category_id' => null,
                    'sort' => 2,
                    'key' => 'project',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => 13,
                    'category_id' => null,
                    'sort' => 3,
                    'key' => 'project',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => null,
                    'category_id' => null,
                    'sort' => 5,
                    'key' => 'recruitment',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ],
                [
                    'parent_id' => null,
                    'category_id' => null,
                    'sort' => 6,
                    'key' => 'contact',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]
            ]);

            MenuTranslation::insert([
                [
                    'menu_id' => 1,
                    'locale' => 'vi',
                    'name' => 'Trang chủ',
                    'description' => null,
                    'link' => '/',
                ],
                [
                    'menu_id' => 1,
                    'locale' => 'en',
                    'name_en' => 'Home',
                    'description' => null,
                    'link' => '/',
                ],

                [
                    'menu_id' => 2,
                    'locale' => 'vi',
                    'name' => 'Giới thiệu',
                    'description' => null,
                    'link' => '/gioi-thieu',
                ],
                [
                    'menu_id' => 2,
                    'locale' => 'en',
                    'name_en' => 'Introduce',
                    'description' => null,
                    'link' => '/introduce',
                ],

                [
                    'menu_id' => 3,
                    'locale' => 'vi',
                    'name' => 'Tầm nhìn',
                    'description' => null,
                    'link' => '/tam-nhin',
                ],
                [
                    'menu_id' => 3,
                    'locale' => 'en',
                    'name_en' => 'Vision',
                    'description' => null,
                    'link' => '/vision',
                ],

                [
                    'menu_id' => 4,
                    'locale' => 'vi',
                    'name' => 'Chiến lược',
                    'description' => null,
                    'link' => '/chien-luoc',
                ],
                [
                    'menu_id' => 4,
                    'locale' => 'en',
                    'name_en' => 'Strategy',
                    'description' => null,
                    'link' => '/strategy',
                ],

                [
                    'menu_id' => 5,
                    'locale' => 'vi',
                    'name' => 'Sứ mệnh',
                    'description' => null,
                    'link' => '/su-menh',
                ],
                [
                    'menu_id' => 5,
                    'locale' => 'en',
                    'name_en' => 'Mission',
                    'description' => null,
                    'link' => '/mission',
                ],

                [
                    'menu_id' => 6,
                    'locale' => 'vi',
                    'name' => 'Giá trị cốt lõi',
                    'description' => null,
                    'link' => '/gia-tri-cot-loi',
                ],
                [
                    'menu_id' => 6,
                    'locale' => 'en',
                    'name_en' => 'Core values',
                    'description' => null,
                    'link' => '/core-values',
                ],

                [
                    'menu_id' => 7,
                    'locale' => 'vi',
                    'name' => 'Thương hiệu',
                    'description' => null,
                    'link' => '/thuong-hieu',
                ],
                [
                    'menu_id' => 7,
                    'locale' => 'en',
                    'name_en' => 'Trademark',
                    'description' => null,
                    'link' => '/trademark',
                ],

                [
                    'menu_id' => 8,
                    'locale' => 'vi',
                    'name' => 'Sơ đồ tổ chức',
                    'description' => null,
                    'link' => '/so-do-to-chuc',
                ],
                [
                    'menu_id' => 8,
                    'locale' => 'en',
                    'name_en' => 'Organizational chart',
                    'description' => null,
                    'link' => '/organizational-chart',
                ],

                [
                    'menu_id' => 9,
                    'locale' => 'vi',
                    'name' => 'Tin tức',
                    'description' => null,
                    'link' => '/tin-tuc',
                ],
                [
                    'menu_id' => 9,
                    'locale' => 'en',
                    'name_en' => 'News',
                    'description' => null,
                    'link' => '/news',
                ],

                [
                    'menu_id' => 10,
                    'locale' => 'vi',
                    'name' => 'Tin Khang Chi',
                    'description' => null,
                    'link' => '/tin-khang-chi',
                ],
                [
                    'menu_id' => 10,
                    'locale' => 'en',
                    'name_en' => 'Khang Chi news',
                    'description' => null,
                    'link' => '/khang-chi-news',
                ],

                [
                    'menu_id' => 11,
                    'locale' => 'vi',
                    'name' => 'Tin thị trường',
                    'description' => null,
                    'link' => '/tin-thi-truong',
                ],
                [
                    'menu_id' => 11,
                    'locale' => 'en',
                    'name_en' => 'Market news',
                    'description' => null,
                    'link' => '/maket-news',
                ],

                [
                    'menu_id' => 12,
                    'locale' => 'vi',
                    'name' => 'Thư viện',
                    'description' => null,
                    'link' => '/thu-vien',
                ],
                [
                    'menu_id' => 12,
                    'locale' => 'en',
                    'name_en' => 'Library',
                    'description' => null,
                    'link' => '/library',
                ],

                [
                    'menu_id' => 13,
                    'locale' => 'vi',
                    'name' => 'Dự án',
                    'description' => null,
                    'link' => '/du-an',
                ],
                [
                    'menu_id' => 13,
                    'locale' => 'en',
                    'name_en' => 'Project',
                    'description' => null,
                    'link' => '/project',
                ],

                [
                    'menu_id' => 14,
                    'locale' => 'vi',
                    'name' => 'Bất động sản cao cấp',
                    'description' => null,
                    'link' => '/bat-dong-san-cao-cap',
                ],
                [
                    'menu_id' => 14,
                    'locale' => 'en',
                    'name_en' => 'Luxury real estate',
                    'description' => null,
                    'link' => '/luxury-real-estate',
                ],

                [
                    'menu_id' => 15,
                    'locale' => 'vi',
                    'name' => 'Bất động sản nghỉ dưỡng',
                    'description' => null,
                    'link' => '/bat-dong-san-nghi-duong',
                ],
                [
                    'menu_id' => 15,
                    'locale' => 'en',
                    'name_en' => 'Resort real estate',
                    'description' => null,
                    'link' => '/resort-real-estate',
                ],

                [
                    'menu_id' => 16,
                    'locale' => 'vi',
                    'name' => 'Đầu tư tài chính',
                    'description' => null,
                    'link' => '/dau-tu-tai-chinh',
                ],
                [
                    'menu_id' => 16,
                    'locale' => 'en',
                    'name_en' => 'Financial investment',
                    'description' => null,
                    'link' => '/financial-investment',
                ],

                [
                    'menu_id' => 17,
                    'locale' => 'vi',
                    'name' => 'Tuyển dụng',
                    'description' => null,
                    'link' => '/tuyen-dung',
                ],
                [
                    'menu_id' => 17,
                    'locale' => 'en',
                    'name_en' => 'Recruitment',
                    'description' => null,
                    'link' => '/recruitment',
                ],

                [
                    'menu_id' => 18,
                    'locale' => 'vi',
                    'name' => 'Liên hệ',
                    'description' => null,
                    'link' => '/lien-he',
                ],
                [
                    'menu_id' => 18,
                    'locale' => 'en',
                    'name_en' => 'Contact',
                    'description' => null,
                    'link' => '/contact',
                ],
            ]);
        }

        $this->enableForeignKeys();
    }
}
