const animateCSS = ($element, animation, prefix = 'animate__') =>
  // We create a Promise and return it
  new Promise((resolve, reject) => {
    const animationName = `${prefix}${animation}`;
    $element.removeClass(`${prefix}fadeOut`).removeClass(`${prefix}fadeInLeft`).removeClass(`${prefix}fadeInRight`)
    // When the animation ends, we clean the classes and resolve the Promise
    function handleAnimationEnd() {
      console.log("vao...")
      //$element.removeClass(`${prefix}animated`).removeClass(animationName);
      resolve('Animation ended');
    }

    $element.one('animationend', handleAnimationEnd);

    $element.addClass(`${prefix}animated`).addClass(animationName);
    
  });

module.exports = animateCSS;
