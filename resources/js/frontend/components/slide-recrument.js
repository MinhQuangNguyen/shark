const animateCSS = require('./animate')

function recrument() {
  $(".slide-recrument").each(function () {
    const $parent = $(this);
    const itemLength = $parent.find(".slide-recrument-image-item").length;
    const viewLeftWidth = $parent.find(".slide-recrument-image-group").width();

    
    $parent.find(".slide-recrument-image-wrap").width(itemLength * viewLeftWidth);
    $parent.find(".slide-recrument-image-item").width(viewLeftWidth);
    $parent.find(".currentIndex").html('01');
    $parent.find(".total").html(('0' + (itemLength)).slice(-2));

    move(0)
    
    $(this).find(".btn-next").off("click").on("click", function () {
      if ($parent.attr("data-scrolling") == '1') {
        return false;
      }
      let index = Number($parent.attr("data-index")) || 0

      //tăng index.
      if (index >= itemLength - 1) index = 0;
      else index++;

      let nextIndex = index + 1;
      if (nextIndex >= itemLength) nextIndex = 0;

      move(index, nextIndex)
    })

    $(this).find(".btn-back").off("click").on("click", function () {
      if ($parent.attr("data-scrolling") == '1') {
        return false;
      }
      let index = Number($parent.attr("data-index")) || 0

      //giảm index.
      if (index == 0) index = itemLength - 1;
      else index--;

      let nextIndex = index + 1;
      if (nextIndex >= itemLength) nextIndex = 0;

      move(index, nextIndex)
    })

    function move(index) {
      if ($parent.attr("data-scrolling") == '1') {
        return false;
      }
      $parent.attr("data-scrolling", '1');

      $parent.find(".currentIndex").html(('0' + (index + 1)).slice(-2));
      //slide content trái
      $parent.attr("data-index", index).find(".slide-recrument-image-wrap").css({
        left: -index * viewLeftWidth
      });

      //change text
      const $content = $(".slide-recrument-content");
      const $newElement = $($parent.find(".slide-recrument-content-item")[index])
      $content.css({ minHeight: $newElement.height() })
      animateCSS($content.find(".animate__fadeIn"), "fadeOut")
      animateCSS($newElement, "fadeIn")
      

      $parent.attr("data-scrolling", '0');
    }
  });
}

$("document").ready(function () {
  recrument();

  $(window).resize(function () {
    recrument();
  })
})