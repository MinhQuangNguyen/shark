require('./components/slide-text')
require('./components/menu-vertical')
require('./components/slide-recrument')

function header() {
  //click button search
  $(".search-btn").click(function () {
    $(".search-inp").toggleClass("active");
    $(".navbar-collapse.menu").toggleClass("onSearch")
    //headerMenu()
  })
}
function headerMenu() {
  //change margin of menu
  setTimeout(function () {
    const containerWidth = $(".right-menu").offset().left - (
      $(".logo").offset().left + $(".logo").outerWidth()
    )

    const menuWidth = $(".menu .navbar-nav").width();
    const margin = (containerWidth - menuWidth) / 2;
    // console.log(containerWidth, menuWidth, margin)
    $(".menu").css({
      marginLeft: $(".logo").outerWidth() + margin - 45 + "px"
    })
  }, 300)

}

function homeSlide() {
  $('.slide-kc .carousel').on('slide.bs.carousel', function (e) {
    const index = e.to;
    $(this).parents(".slide-kc").find(".title-group .active").removeClass("active")
    $($(this).parents(".slide-kc").find(".title-group .item")[index]).addClass("active")

  })
}

function partnerSlide() {
  $(".partner-home-slide").slick({
    dots: false,
    infinite: true,
    mobileFirst: true,
    // variableWidth: true,
    speed: 300,
    slidesToShow: 1,
    prevArrow: $('.partner-prev'),
    nextArrow: $('.partner-next'),

    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4
        }
      }
    ]
  })
}

function utilitesSlide() {
  $(".project-home-slide").slick({
    dots: false,
    infinite: true,
    mobileFirst: true,
    // variableWidth: true,
    speed: 300,
    slidesToShow: 3,
    prevArrow: $('.utilities-prev'),
    nextArrow: $('.utilities-next'),

    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  })
  $(".slide-show-modal").on("click", ".item", function () {
    const $parent = $(this).parents(".slide-show-modal");
    const key = $(this).data("key");
    const modal = $parent.data("target");
    $(modal).carousel(key)
  })
}

function homeScopeHover() {
  $(".project-item").hover(function () {
    const bottomLine = $(this).find(".bottom-line");
    const bottomLineWidth = $(bottomLine).width();
    const bottomHoverWidth = $(bottomLine).attr("data-width-hover");
    $(bottomLine).attr("data-width", bottomLineWidth).width(bottomLineWidth + Number(bottomHoverWidth))
  }, function () {
    const bottomLine = $(this).find(".bottom-line");
    const bottomWidth = $(bottomLine).attr("data-width");
    $(bottomLine).width(bottomWidth)
  })
  $(window).resize(function () {
    $(".project-item .bottom-line").removeAttr("data-width").removeAttr("style");
  })
}

function menuMobile() {
  //click submenu
  $(".nav-item.has-sub-menu").click(function () {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active").find(".sub-menu").stop(true, true).slideUp();
    } else {
      $(this).addClass("active").find(".sub-menu").stop(true, true).slideDown();
    }
  })

  //click show menu mobile
  $(".navbar-toggler").click(function () {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active")
      $(".menu-mobile").stop(true, true).fadeOut({
        duration: 300
      });
    } else {
      $(this).addClass("active")
      $(".menu-mobile").stop(true, true).fadeIn({
        duration: 300
      });
    }
  })
  $(window).resize(function () {
    $(this).removeClass("active")
    $(".menu-mobile").stop(true, true).fadeOut({
      duration: 300
    });
  });
}

function changeSelectProject() {
  $('.search-select.project').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    window.location.href = "/" + $(this).val()
  });
}

function changeSelectPost() {
  $('.search-select.post').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    window.location.href = "/" + $(this).val()
  });
}

function changeSelectRecruitment() {
  $('.search-select.recruitment').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    this.form.submit();
  });
}

function changeTabField() {
  //active lại tab theo url
  const $fieldPage = $(".tab-field-main");
  if ($fieldPage.length) {
    const pathName = window.location.pathname;
    $fieldPage.find(`.nav-tabs .nav-item[data-url='${pathName}']`).tab("show")
  }
  //điều chỉnh lại hover trên menu và slide.
  $(".tab-field-main").off("click").on("click", ".nav-item", function () {
    const newBanner = $(this).data("banner");
    const newName = $(this).data("name");
    const newLink = $(this).data("url");
    const navActive = $('.nav-item.dropdown.active')
    if (newLink) {
      $("#slide-field").find('.img').css('background-image', 'url(' + newBanner + ')')
      $("#slide-field").find('.title').text(newName)
      navActive.find('.active').removeClass('active')
      navActive.find(`[data-href='${newLink}']`).addClass('active')
      window.history.pushState('', '', newLink);
    }
  })
}

function aboutScollTop() {
  setTimeout(function () {
    const hash = window.location.hash;
    const $aboutContent = $(".item-body-about")
    if (!$aboutContent.length) return;
    if (hash == "#noscroll") return;
    $("html, body").animate({
      scrollTop: $aboutContent.offset().top
    }, 500)
  }, 200)
}

$("document").ready(function () {
  header();
  //headerMenu();
  homeSlide();
  menuMobile();
  homeScopeHover();
  partnerSlide();
  utilitesSlide();
  changeSelectProject();
  changeSelectPost();
  changeSelectRecruitment();
  changeTabField();
  aboutScollTop();
})

$(window).resize(function () {
  headerMenu()
})
