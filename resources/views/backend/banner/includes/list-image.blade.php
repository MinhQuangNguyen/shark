<div x-data="initData()" x-init="init()">
    <template x-for="(item, index) in items" :key="index">
        <div class="component-item">
            <div class="form-group row">
                <div class="col-md-8">
                    <div class="form-group row">
                        <label for="title" class="col-md-2 col-form-label">@lang('Title')</label>

                        <div class="col-md-10">
                            <input type="text" x-bind:name="`slides[${index}][title]`" x-on:input.debounce.250="changeInput($event.target.value, index, 'title')" class="form-control" placeholder="{{ __('Title') }}" x-bind:value="item.title" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="title_en" class="col-md-2 col-form-label">@lang('Title (En)')</label>

                        <div class="col-md-10">
                            <input type="text" x-bind:name="`slides[${index}][title_en]`" x-on:input.debounce.250="changeInput($event.target.value, index, 'title_en')" class="form-control" placeholder="{{ __('Title (En)') }}" x-bind:value="item.title_en" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="content" class="col-md-2 col-form-label">@lang('Content')</label>

                        <div class="col-md-10">
                            <textarea type="text" x-bind:name="`slides[${index}][content]`" class="content-images" x-on:input.debounce.250="changeInput($event.target.value, index, 'content')" class="form-control" placeholder="{{ __('Content') }}" x-bind:value="item.content"></textarea>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="content_en" class="col-md-2 col-form-label">@lang('Content (En)')</label>

                        <div class="col-md-10">
                            <textarea type="text" x-bind:name="`slides[${index}][content_en]`" class="content-images" x-on:input.debounce.250="changeInput($event.target.value, index, 'content_en')" class="form-control" placeholder="{{ __('Content (En)') }}" x-bind:value="item.content_en"></textarea>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="link" class="col-md-2 col-form-label">@lang('Link')</label>

                        <div class="col-md-10">
                            <input type="text" x-bind:name="`slides[${index}][link]`" x-on:input.debounce.250="changeInput($event.target.value, index, 'link')" class="form-control" placeholder="{{ __('Link') }}" x-bind:value="item.link" />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="link_en" class="col-md-2 col-form-label">@lang('Link (En)')</label>

                        <div class="col-md-10">
                            <input type="text" x-bind:name="`slides[${index}][link_en]`" x-on:input.debounce.250="changeInput($event.target.value, index, 'link_en')" class="form-control" placeholder="{{ __('Link (En)') }}" x-bind:value="item.link_en" />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="content_en" class="col-md-2 col-form-label">@lang('Sort')</label>
                        <div class="col-md-10">
                            <input type="number" x-bind:name="`slides[${index}][sort]`" x-on:input.debounce.250="changeInput($event.target.value, index, 'sort')" class="form-control" placeholder="{{ __('Sort') }}" x-bind:value="item.sort" />
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-center active text-muted form-control" style="height: auto;">
                        <img class="cursor img-thumbnail"
                            x-bind:id="`slides[${index}]`" height="225" width="225"
                            x-bind:src="item.image ? `${item.image}` : `{{ url('img/no_image.png') }}`"
                            x-on:click="selectImg(`slides[${index}]`, `slides[${index}][image]`, index)"
                        />
                        <input type="hidden" x-bind:value="item.image" x-bind:name="`slides[${index}][image]`" x-bind:id="`slides[${index}][image]`" />
                    </div>
                </div>
            </div>
            <div class="text-right">
                <button type="button" class="btn btn-outline-primary mr-1" @click="addItem()">
                    <i class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-outline-danger" x-show="items.length > 1" @click="removeItem(index)">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
    </template>
</div>

@push('after-scripts')
<script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('js/ckeditor/ckeditor_replace.js')}}"></script>
<script type="text/javascript">
    function initData() {
        return {
            items: [],
            addItem() {
                let data = [...this.items, {'title': '', 'title_en': '', 'content': '', 'content_en': '', 'link': '', 'link_en': '', 'image': '/img/no_image.png', 'sort': '' }];
                this.items = data
            },
            removeItem(index) {
                let data = this.items.splice(index, 1);
                this.items = this.items
            },
            changeInput(value, index, type) {
                let obj = {}
                obj[type] = value
                let itemNew = { ...this.items[index], ...obj }
                this.items[index] = itemNew
            },
            selectImg(imgId, inputId, index) {
                let trash = imgId.substring(3, 4);
                let _this = this
                CKFinder.modal({
                    height: 550,
                    chooseFiles: true,
                    onInit: function(finder) {
                        finder.on('files:choose', function(evt) {
                            var file = evt.data.files.first();
                            document.getElementById(imgId).src = file.getUrl();
                            document.getElementById(inputId).value = file.getUrl();
                            _this.changeInput(file.getUrl(), index, 'image')
                            $("#trash"+trash).show();
                        });
                        finder.on('file:choose:resizedImage', function(evt) {
                            document.getElementById(imgId).src = evt.data.resizedUrl;
                            document.getElementById(inputId).value = evt.data.resizedUrl;
                            $("#trash"+trash).show();
                        });
                    }
                });
            },
            init() {
                let slides = @json($slides);
                this.items = slides.length ? slides : [{'title': '', 'title_en': '', 'content': '', 'content_en': '', 'link': '', 'link_en': '', 'image': '/img/no_image.png', 'sort': '' }]
            },
        }
    }
</script>
@endpush('after-scripts')
