@inject('model', '\App\Models\Post')

@extends('backend.layouts.app')

@section('title', __('Create Post'))

@section('content')
    <x-forms.post :action="route('admin.post.store')" enctype="multipart/form-data">
        <x-backend.card>
            <x-slot name="header">
                @lang('Create Post')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.post.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div x-data="{type : '{{ $model::TYPE_NEW_IMAGE }}'}">
                    <div class="form-group row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label for="title" class="col-md-2 col-form-label">@lang('Title')</label>

                                <div class="col-md-10">
                                    <input type="text" name="title" class="form-control" placeholder="{{ __('Title') }}" value="{{ old('title') }}" maxlength="255" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="title_en" class="col-md-2 col-form-label">@lang('Title (En)')</label>

                                <div class="col-md-10">
                                    <input type="text" name="title_en" class="form-control" placeholder="{{ __('Title (En)') }}" value="{{ old('title_en') }}" maxlength="255" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description_short" class="col-md-2 col-form-label">@lang('Description Short')</label>

                                <div class="col-md-10">
                                    <textarea type="text" name="description_short" class="form-control" placeholder="{{ __('Description Short') }}">{{ old('description_short') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description_short_en" class="col-md-2 col-form-label">@lang('Description Short (En)')</label>

                                <div class="col-md-10">
                                    <textarea type="text" name="description_short_en" class="form-control" placeholder="{{ __('Description Short (En)') }}">{{ old('description_short_en') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="slug" class="col-md-2 col-form-label">@lang('Slug')</label>

                                <div class="col-md-10">
                                    <input type="text" name="slug" class="form-control" placeholder="{{ __('Slug') }}" value="{{ old('slug') }}" maxlength="255" required />
                                </div>
                            </div>

                            <div class="form-group row link">
                                <label for="link" class="col-md-2 col-form-label">@lang('Link Video')</label>

                                <div class="col-md-10">
                                    <input type="text" name="link" class="form-control" placeholder="{{ __('Link Video') }}" value="{{ old('link') }}" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="slug_en" class="col-md-2 col-form-label">@lang('Slug (En)')</label>

                                <div class="col-md-10">
                                    <input type="text" name="slug_en" class="form-control" placeholder="{{ __('Slug (En)') }}" value="{{ old('slug_en') }}" maxlength="255" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="content" class="col-md-2 col-form-label">@lang('Content')</label>

                                <div class="col-md-10">
                                    <textarea type="text" name="content" id="content" class="form-control" placeholder="{{ __('Content') }}">{{ old('content') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="content_en" class="col-md-2 col-form-label">@lang('Content (En)')</label>

                                <div class="col-md-10">
                                    <textarea type="text" name="content_en" id="content_en" class="form-control" placeholder="{{ __('Content (En)') }}">{{ old('content_en') }}</textarea>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="images-preview-div">
                                        <input accept="image/*" type="file" id="images" name="image" style="display: none" />
                                        <label for="images" class="w-100 text-center">
                                            <img src="http://www.placehold.it/300x300/EFEFEF/AAAAAA&amp;text=No+Image" alt="No Image" id="image-pre" class="img-thumbnail">
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="category_id" class="col-md-12 col-form-label">@lang('Category')</label>

                                <div class="col-md-12">
                                    <select name="category_id" class="form-control">
                                        @foreach($categories as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><!--form-group-->

                            <div class="form-group row">
                                <label for="type" class="col-md-12 col-form-label">@lang('Type')</label>

                                <div class="col-md-12">
                                    <select name="type" class="form-control" required x-on:change="type = $event.target.value">
                                        <option value="{{ $model::TYPE_NEW_IMAGE }}" selected>@lang('New, Image')</option>
                                        <option value="{{ $model::TYPE_VIDEO }}">@lang('Video')</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sort" class="col-md-12 col-form-label">@lang('Sort')</label>

                                <div class="col-md-12">
                                    <input type="number" name="sort" class="form-control" placeholder="{{ __('Sort') }}" value="{{ old('sort') }}" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="banner" class="col-md-2 col-form-label">@lang('Banner')</label>

                                <div class="col-md-10">
                                    <div class="custom-file">
                                        <input type="file" name="banner" class="custom-file-input" id="customFile" required />
                                        <label class="custom-file-label" for="customFile">{{ __('Choose file') }}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="is_highlight" class="col-md-2 col-form-label">@lang('Highlight')</label>

                                <div class="col-md-10">
                                    <div class="form-check">
                                        <input
                                            type="checkbox"
                                            name="is_highlight"
                                            value="1"
                                            class="form-check-input checkbox"
                                        />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="active" class="col-md-12 col-form-label">@lang('Status')</label>

                                <div class="col-md-12" style="line-height: 32px;">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="active" id="active1" value="{{ $model::ACTIVE }}" checked>
                                        <label class="form-check-label" for="active1">@lang('Active')</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="active" id="active0" value="{{ $model::INACTIVE }}">
                                        <label class="form-check-label" for="active0">@lang('Inactive')</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Create')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>

    @push('after-scripts')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('js/ckeditor/ckeditor_replace.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("select").select2({
                placeholder: "{{ __('Select a option') }}",
                allowClear: true
            });

            var previewImages = function(input, imgPreviewPlaceholder) {
                if (input.files) {
                    var filesAmount = input.files.length;
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
                        reader.onload = function(event) {
                            $('#image-pre').attr('src', event.target.result);
                        }
                        reader.readAsDataURL(input.files[i]);
                    }
                }
            };
            $('#images').on('change', function() {
                previewImages(this, 'div.images-preview-div');
            });
            $('input[name="banner"]').change(function(e){
                var fileName = e.target.files[0].name;
                $('.custom-file-label').html(fileName);
            });
        });
    </script>
    @endpush('after-scripts')
@endsection
