@extends('frontend.layouts.khangchi')

@section('title', __('strategy'))
@section('content')
    @include('frontend.about.includes.banner')
  <div class="item-body-about">
        <div class="item-vision-main-mqn">
            <div class="container">
                <div class="row">
                    <div class="col-14 col-lg-4">
                        @include('frontend.includes.menu-vertica-about')
                    </div>
                    <div class="col-14 col-lg-10">
                        <div class="item-info-banner-brand">
                           <div class="item-img-brand">
                                <img src="/img/img14.png" alt="">
                           </div>
                           <div class="item-text-brand">
                                <div class="title">Câu chuyện thương hiệu</div>
                                <div class="text">Kỳ lân biểu tượng cho sự may mắn, tốt lành và khôn ngoan; chìa khóa hoa mai là kết hợp mang tính biểu tượng của chữ Khang (thịnh vượng) và Chi (cành Mai). Thiết kế tượng trưng cho mong muốn cũng như khẳng định sự tiếp nối, kế thừa và phát triển bền vững của doanh nghiệp.</div>
                                <div class="text">Ngoài ra, hình ảnh Kỳ lân còn mang khát vọng, niềm tin về một công ty “kỳ lân” (khái niệm kỳ lân unicorn dùng để chỉ các start up được định giá tuè 1 tỉ USD trở lên). Và trong truyền thuyết, khi kỳ lân xuất hiện sẽ là sự báo trước về điềm lành, thái bình thịnh vượng.</div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
