@extends('frontend.layouts.khangchi')

@section('title', __('strategy'))
@section('content')
    @include('frontend.about.includes.banner')
    <div class="item-body-about">
        <div class="item-vision-main-mqn">
            <div class="container">
                <div class="row">
                    <div class="col-14 col-lg-4">
                        @include('frontend.includes.menu-vertica-about')
                    </div>
                    <div class="col-14 col-lg-10">
                        <div class="item-info-banner-coreValue">
                            <div class="row">
                                <div class="col-14 col-lg-7">
                                    <div class="title">Giá trị cốt lõi</div>
                                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
                                </div>
                                <div class="col-14 col-lg-7">
                                    <img src="/img/imgCoreValue.svg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
