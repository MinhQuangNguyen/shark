<div class="d-block d-lg-none">
    <div id="header-banner-about">
        <div class="container-fluid">
            <div class="backgroundRed">
                <div class="row no-gutters">
                    <div class="col-14 col-lg-7">
                        <div class="info-header-vision">
                            <div class="title">{{ $about['name'] }}</div>
                            {!! $about['description'] !!}
                        </div>
                    </div>
                    <div class="col-14 col-lg-7">
                        <div class="about-background">
                            <img src="{{ asset('img/imgAbout.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="d-none d-lg-block">
    <div id="header-banner-about">
        <div class="container-fluid">
            <div class="backgroundRed">
                <div class="row no-gutters align-items-end">
                    <div class="col-14 col-lg-7">
                        <div class="info-header-vision">
                            <img src="{{ asset('img/imgAbout.png') }}" alt="">
                        </div>
                    </div>
                    <div class="col-14 col-lg-7">
                        <div class="about-background">
                            <div class="title">{{ $about['name'] }}</div>
                            {!! $about['description'] !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
