@extends('frontend.layouts.khangchi')

@section('title', __('strategy'))
@section('content')
    @include('frontend.about.includes.banner')
    <div class="item-body-about">
        <div class="item-vision-main-mqn">
            <div class="container">
                <div class="row">
                    <div class="col-14 col-lg-4">
                        <div class="item-vertical-about">
                            @include('frontend.includes.menu-vertica-about', ["menu" => $subMenus])
                        </div>
                        <div class="item-background-about-left"></div>

                    </div>
                    <div class="col-14 col-lg-10">
                        <div class="menu-vertical-content">
                            @foreach ($subMenus as $subMenu)
                                <div class="content-{{ $subMenu->id }} {{$subMenu->isActive? 'active': ''}} animate__animated animate__fadeOut">
                                    {!! $subMenu->content !!}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
