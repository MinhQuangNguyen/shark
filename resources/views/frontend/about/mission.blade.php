@extends('frontend.layouts.khangchi')

@section('title', __('strategy'))
@section('content')
    @include('frontend.about.includes.banner')
    <div class="item-body-about">
        <div class="item-vision-main-mqn">
            <div class="container">
                <div class="row">
                    <div class="col-14 col-lg-4">
                        @include('frontend.includes.menu-vertica-about')
                    </div>
                    <div class="d-block d-lg-none">
                        <div class="col-14 col-lg-14">
                            <div class="item-info-banner-vision">
                                <div class="row">
                                    <div class="col-4 col-lg-2">
                                        <div class="icon-info-vision">
                                            <img src="/img/iconLogoRed.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-10  col-lg-12">
                                        <div class="item-info-vision-main">
                                            <div class="title-item-about">Sứ mệnh</div>
                                            <p>Đem đến cho khách hàng những sản phẩm, dịch vụ ưu việt và thiết kế riêng biệt, phù hợp nhất với nhu cầu khách hàng.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-info-mision-more">
                                    <div class="item-more-mission">
                                        <div class="title">Đối với khách hàng</div>
                                        <div class="text">Đem đến cho khách hàng những sản phẩm, dịch vụ ưu việt và thiết kế riêng biệt, phù hợp nhất với nhu cầu khách hàng.</div>
                                    </div>
            
                                    <div class="item-more-mission">
                                        <div class="title">Đối với đối tác</div>
                                        <div class="text">Vun đắp niềm tin, tạo lập mối quan hệ đối tác lâu dài, bền vững và cùng có lợi.</div>
                                    </div>
            
                                    <div class="item-more-mission">
                                        <div class="title">Đối với người lao động</div>
                                        <div class="text">Xây dựng môi trường làm việc thân thiện, chuyên nghiệp và sáng tạo – nơi mà mọi nhân viên đều có cơ hội phát huy tối đa năng lực và sở trường, cùng đóng góp giá trị cho tổ chức, chia sẻ thành quả và phát triển sự nghiệp của bản thân.</div>
                                    </div>
            
                                    <div class="item-more-mission">
                                        <div class="title">Đối với cộng đồng</div>
                                        <div class="text">Góp phần gia tăng chất lượng cuộc sống, nỗ lực đóng góp vào sự phát triển bền vững của cộng đồng và của đất nước.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="d-none d-lg-block">
                        <div class="col-14 col-lg-14">
                            <div class="item-info-banner-vision">
                                <div class="row">
                                    <div class="col-4 col-lg-2">
                                        <div class="icon-info-vision">
                                            <img src="/img/iconLogoRed.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-10  col-lg-10">
                                        <div class="item-info-vision-main">
                                            <div class="title-item-about">Sứ mệnh</div>
                                            <p>Đem đến cho khách hàng những sản phẩm, dịch vụ ưu việt và thiết kế riêng biệt, phù hợp nhất với nhu cầu khách hàng.</p>
                                        </div>
                                        <div class="item-info-mision-more">
                                            <div class="item-more-mission">
                                                <div class="title">Đối với khách hàng</div>
                                                <div class="text">Đem đến cho khách hàng những sản phẩm, dịch vụ ưu việt và thiết kế riêng biệt, phù hợp nhất với nhu cầu khách hàng.</div>
                                            </div>
                    
                                            <div class="item-more-mission">
                                                <div class="title">Đối với đối tác</div>
                                                <div class="text">Vun đắp niềm tin, tạo lập mối quan hệ đối tác lâu dài, bền vững và cùng có lợi.</div>
                                            </div>
                    
                                            <div class="item-more-mission">
                                                <div class="title">Đối với người lao động</div>
                                                <div class="text">Xây dựng môi trường làm việc thân thiện, chuyên nghiệp và sáng tạo – nơi mà mọi nhân viên đều có cơ hội phát huy tối đa năng lực và sở trường, cùng đóng góp giá trị cho tổ chức, chia sẻ thành quả và phát triển sự nghiệp của bản thân.</div>
                                            </div>
                    
                                            <div class="item-more-mission">
                                                <div class="title">Đối với cộng đồng</div>
                                                <div class="text">Góp phần gia tăng chất lượng cuộc sống, nỗ lực đóng góp vào sự phát triển bền vững của cộng đồng và của đất nước.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="item-background-content-mission"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
