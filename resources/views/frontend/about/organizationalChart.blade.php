@extends('frontend.layouts.khangchi')

@section('title', __('organizationalChart'))
@section('content')
    @include('frontend.about.includes.banner')
    <div class="item-body-about">
        <div class="item-vision-main-mqn">
            <div class="container">
                <div class="row">
                    <div class="col-14 col-lg-4">
                        @include('frontend.includes.menu-vertica-about')
                    </div>
                    <div class="col-14 col-lg-10">
                        <div class="item-info-banner-chart">
                            <div class="title">Sơ đồ mô hình tập đoàn Hasco</div>
                            <div class="text">Theo Quyết định số 42/2019/QĐ - Hasco, ban hành ngày 01/11/2019</div>
                            <img src="/img/imgSodoMB.svg" alt="" class="d-block d-lg-none">
                            <img src="/img/imgSodo.svg" alt="" class="d-none d-lg-block">
                            <p class="d-block d-lg-none">(Click để phóng to)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
