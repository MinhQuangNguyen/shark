@extends('frontend.layouts.khangchi')

@section('title', __('strategy'))
@section('content')
    @include('frontend.about.includes.banner')
    <div class="item-body-about">
        <div class="item-vision-main-mqn">
            <div class="container">
                <div class="row">
                    <div class="col-14 col-lg-4">
                        @include('frontend.includes.menu-vertica-about')
                    </div>
                    <div class="col-14 col-lg-10">
                        <div class="item-info-banner-strategy">
                            <div class="item-info-strategy-main">
                                <div class="item-info-strategy">
                                    <div class="row">
                                        <div class="col-14 col-lg-7">
                                            <div class="img-info-strategy">
                                                <img src="/img/img13.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-14 col-lg-7">
                                            <div class="text-info-strategy">
                                                <div class="title-info-strategy">01</div>
                                                <div class="title-info-desc">Kiến tạo những công trình, dự án bền vững ngàn đời</div>
                                                <p>Mục tiêu của Tập đoàn Khang Chi là nâng cao chất lượng cuộc sống thông qua các công trình, dự án với kiến trúc độc đáo và không gian nghỉ dưỡng hoàn hảo. Sứ mệnh là người tiên phong, thiết kế và phát triển những không gian tạo cảm giác khác biệt, làm phong phú cuộc sống của con người trong mọi khía cạnh và tồn tại bền vững cho những thế hệ sau.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-info-strategy d-none d-lg-block">
                                    <div class="row">
                                       
                                        <div class="col-14 col-lg-7">
                                            <div class="text-info-strategy">
                                                <div class="title-info-strategy">02</div>
                                                <div class="title-info-desc">Tạo nên chất lượng dịch vụ cao cấp</div>
                                                <p>Tập đoàn Khang Chi luôn tâm niệm tạo nên chất lượng dịch vụ cao cấp nhất trong mọi lĩnh vực để mang đến sự hài lòng tuyệt đối của khách hàng.</p>
                                            </div>
                                        </div>
                                         <div class="col-14 col-lg-7">
                                            <div class="img-info-strategy">
                                                <img src="/img/img12.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="item-info-strategy d-block d-lg-none">
                                    <div class="row">
                                        <div class="col-14 col-lg-7">
                                            <div class="img-info-strategy">
                                                <img src="/img/img12.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-14 col-lg-7">
                                            <div class="text-info-strategy">
                                                <div class="title-info-strategy">02</div>
                                                <div class="title-info-desc">Tạo nên chất lượng dịch vụ cao cấp</div>
                                                <p>Tập đoàn Khang Chi luôn tâm niệm tạo nên chất lượng dịch vụ cao cấp nhất trong mọi lĩnh vực để mang đến sự hài lòng tuyệt đối của khách hàng.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-info-strategy">
                                    <div class="row">
                                        <div class="col-14 col-lg-7">
                                            <div class="img-info-strategy">
                                                <img src="/img/img3.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-14 col-lg-7">
                                            <div class="text-info-strategy">
                                                <div class="title-info-strategy">03</div>
                                                <div class="title-info-desc">Mở rộng kinh doanh gia tăng giá trị Tập đoàn</div>
                                                <p>Mục tiêu đầu tư tài chính của Tập đoàn Khang Chi nhằm huy động và sử dụng vốn có hiệu quả, tham gia đầu tư, góp vốn vào các ngành/lĩnh vực có triển vọng về lợi nhuận và tiềm năng tăng trưởng bền vững trong dài hạn, cung cấp các dịch vụ có chất lượng để tối ưu hóa lợi nhuận. Từ đó tạo thế cân bằng cho chu kỳ kinh doanh, phát triển quỹ đất, đa dạng hóa danh mục tài sản để giảm thiểu rủi ro.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
