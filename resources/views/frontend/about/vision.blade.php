@extends('frontend.layouts.khangchi')

@section('title', __('vision'))
@section('content')
@include('frontend.about.includes.banner')
    <div class="item-body-about">
        <div class="item-vision-main-mqn">
            <div class="container">
                <div class="row">
                    <div class="col-14 col-lg-4">
                        @include('frontend.includes.menu-vertica-about')
                    </div>
                    <div class="col-14 col-lg-10">
                        <div class="item-info-banner-vision">
                            <div class="row">
                                <div class="col-4 col-lg-2">
                                    <div class="icon-info-vision">
                                        <img src="{{asset('img/iconLogoRed.png')}}" alt="">
                                    </div>
                                </div>
                                <div class="col-8  col-lg-5">
                                    <div class="item-info-vision-main">
                                        <div class="title-item-about">Tầm nhìn</div>
                                        <p>Trở thành Tập đoàn kinh tế hàng đầu Việt Nam, tập trung vào lĩnh vực kinh doanh chiến lược là Bất động sản cao cấp, Bất động sản du lịch nghỉ dưỡng và Đầu tư tài chính… với dịch vụ và chất lượng hàng đầu.</p>
                                    </div>
                                </div>
                                <div class="col-14 col-lg-3">
                                    <div class="item-background-content-right"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection