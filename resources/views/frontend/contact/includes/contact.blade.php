<section class="subpage-content">
    <div class="container">
        <div class="row">
            <div class="title col-lg-12">
                <h1 class="reveal blur-fade-down animated">Contact</h1>
                <h2 class="reveal blur-fade-down animated" data-delay="400">We’re always happy to answer shark science
                    questions or to chat with potential new members of our growing community.</h2>
            </div>
        </div>
        <div class="user-content">
            <div class="contact">
                <div class="row">
                    <div class="col-xs-12 col-lg-4 reveal blur-fade-down animated">
                        <div class="contact-data">
                            <div class="contact-text">
                                <h2>Address</h2>
                                <p>The Marine Biological Association<br>
                                    The Laboratory, Citadel Hill<br>
                                    Plymouth, Devon<br>
                                    PL1 2PB, UK</p>
                                <p><a href="">info@globalsharkmovement.org</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-8 reveal blur-fade-up animated">
                        <div class="contact-form">
                            <h2>Give us a shout</h2>
                            <div role="form" class="wpcf7" id="wpcf7-f106-o1" lang="en-US" dir="ltr">
                                <div class="screen-reader-response">
                                    <p role="status" aria-live="polite" aria-atomic="true"></p>
                                    <ul></ul>
                                </div>
                                <form action="" method="post"
                                    class="wpcf7-form init" novalidate="novalidate" data-status="init">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="106">
                                        <input type="hidden" name="_wpcf7_version" value="5.4.2">
                                        <input type="hidden" name="_wpcf7_locale" value="en_US">
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f106-o1">
                                        <input type="hidden" name="_wpcf7_container_post" value="0">
                                        <input type="hidden" name="_wpcf7_posted_data_hash" value=""></div>
                                    <div class="row">
                                        <div class="col-xs-12 col-lg-6">
                                            <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                    name="your-name" value="" size="40"
                                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                    aria-required="true" aria-invalid="false"
                                                    placeholder="Name and surname"></span></div>
                                        <div class="col-xs-12 col-lg-6">
                                            <span class="wpcf7-form-control-wrap phone"><input type="text" name="phone"
                                                    value="" size="40" class="wpcf7-form-control wpcf7-text"
                                                    aria-invalid="false" placeholder="Phone number"></span></div>
                                        <div class="col-xs-12 col-lg-6">
                                            <span class="wpcf7-form-control-wrap company"><input type="text"
                                                    name="company" value="" size="40"
                                                    class="wpcf7-form-control wpcf7-text" aria-invalid="false"
                                                    placeholder="Company name"></span></div>
                                        <div class="col-xs-12 col-lg-6">
                                            <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                    name="your-email" value="" size="40"
                                                    class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                    aria-required="true" aria-invalid="false"
                                                    placeholder="E-mail"></span></div>
                                        <div class="col-lg-12">
                                            <span class="wpcf7-form-control-wrap your-message"><textarea
                                                    name="your-message" cols="40" rows="10"
                                                    class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"
                                                    placeholder="Your Message"></textarea></span></div>
                                        <div class="col-xs-12 col-lg-12">
                                            <input type="submit" value="Send"
                                                class="wpcf7-form-control wpcf7-submit"><span
                                                class="ajax-loader"></span></div>
                                    </div>
                                    <div class="wpcf7-response-output" aria-hidden="true"></div>
                                </form>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
