@extends('frontend.layouts.khangchi')

@section('title', __('Contact'))

@section('content')
    @include('frontend.contact.includes.contact')
@endsection
