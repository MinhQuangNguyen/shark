<?php $lang = (app()->getLocale() === 'vi' ? 'linh-vuc' : 'field') ?>
<div class="tab-field-main">
    <div class="container">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                @if(isset($fields))
                @foreach($fields as $key => $field)
                <a
                    class="nav-item nav-link {{ $key === 0 ? 'active' : ''}}"
                    id="{{ 'tab-'.$field->key }}" data-toggle="tab"
                    data-target="{{ '#nav-'.$field->key }}"
                    role="tab" 
                    aria-controls="{{ 'nav-'.$field->key }}"
                    aria-selected="{{ $key === 0 ? 'true' : 'false'}}"
                    data-url="{{ '/'.$lang.'/'.$field->slug }}"
                    data-banner="{{ asset($field->banner) }}"
                    data-name="{{ $field->name }}"
                >
                    {{ $field->name }}
                </a>
                @endforeach
                @endif
            </div>
        </nav>
    </div>
</div>
