
<section id="about" class="about">
    <div class="shark reveal fade-in-blur-def rellax" data-delay="500" style="transform: translate3d(0px, 0px, 0px);">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-5 about-box rellax-down" style="transform: translate3d(0px, 0px, 0px);">
                <div class="box-line reveal blur-fade-down">
                    <div class="box-line-wrapper">
                        <img src="img/sea.jpg" class="sea-bg" alt=""><canvas width="500" height="1280"
                            style="touch-action: none; max-width: 100%; object-fit: cover; width: 100%; height: 100%; cursor: inherit;"></canvas>
                    </div>
                </div>
                <div class="about-text">
                    <h2 class="reveal blur-enter-left">About</h2>
                    <div class="about-text-big reveal blur-enter-left" data-delay="100">
                        <p>The Global Shark Movement Project (GSMP) is a scientific research project bringing together
                            40
                            shark research teams spread across more than 100 institutes in 26 countries.</p>
                    </div>
                </div>
                <img src="img/shark-about.png" alt="" class="about-shark reveal blur-enter-left" data-delay="200"
                    width="671" height="379" style="touch-action: none; cursor: inherit;">

            </div>
            <div class="col-5 what-box reveal blur-fade-up" data-delay="600">
                <div class="about-text-regular reveal blur-enter-left" data-delay="300">
                    <p>Our aim is to advance scientific knowledge of shark behaviour, ecology, conservation and
                        fisheries
                        science that can be used to inform improved management of threatened sharks and ocean
                        biodiversity.
                        The unique database of shark tracking and environmental data we have assembled by working as a
                        global research community enables new science discoveries that inform policy relating to shark
                        conservation and the sustainable management of shark populations.</p>
                    <p>GSMP was founded in 2016 and is coordinated from the&nbsp;<a href="" target="_blank"
                            rel="noopener">Marine Biological Association Laboratory</a>&nbsp;in Plymouth,UK.
                    </p>
                </div>
                <div class="what-text reveal blur-fade-up">
                    <h2 class="reveal blur-fade-up" data-delay="200">What</h2>
                    <div class="reveal blur-fade-up" data-delay="400">
                        <p>The Global Shark Movement Project – GSMP – is an international scientific research team
                            focused
                            on tracking the movements and changing habitats of pelagic sharks and quantifying the
                            threats they
                            face in order to inform evidence-based conservation</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



