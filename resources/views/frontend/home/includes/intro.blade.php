<section class="intro">
    <div class="world-wrapper">
        <img src="img/header-bg.png" class="world reveal fade-in-blur-def" alt="world" data-delay="100">
        <div class="world-pins">
            <div class="pin pin-1 reveal fade-anim-default" data-delay="1000" style="top:34%; left:37%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-2 reveal fade-anim-default" data-delay="2000" style="top:37%; left:28%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-3 reveal fade-anim-default" data-delay="3000" style="top:53%; left:28%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-4 reveal fade-anim-default" data-delay="4000" style="top:54%; left:39%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-5 reveal fade-anim-default" data-delay="5000" style="top:62%; left:36%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-6 reveal fade-anim-default" data-delay="6000" style="top:69%; left:42%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-7 reveal fade-anim-default" data-delay="7000" style="top:85%; left:39%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-8 reveal fade-anim-default" data-delay="8000" style="top:87%; left:55%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-9 reveal fade-anim-default" data-delay="9000" style="top:76%; left:58.5%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-10 reveal fade-anim-default" data-delay="10000" style="top:66%; left:63%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-11 reveal fade-anim-default" data-delay="11000" style="top:79%; left:79%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-12 reveal fade-anim-default" data-delay="12000" style="top:88%; left:87%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-13 reveal fade-anim-default" data-delay="13000" style="top:81%; left:94%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-14 reveal fade-anim-default" data-delay="14000" style="top:84%; left:100%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-15 reveal fade-anim-default" data-delay="15000" style="top:54%; left:87%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-16 reveal fade-anim-default" data-delay="16000" style="top:43%; left:90%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-17 reveal fade-anim-default" data-delay="17000" style="top:40%; left:5%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-18 reveal fade-anim-default" data-delay="18000" style="top:62%; left:16%">
                <div class="circle"></div>
            </div>
            <div class="pin pin-19 reveal fade-anim-default" data-delay="19000" style="top:82%; left:19%">
                <div class="circle"></div>
            </div>
        </div>
        <div class="intro-text">
            <h1 class="reveal blur-fade-down" data-delay="700"><span>The Global Shark Movement Project</span>
            </h1>
            <img class="reveal blur-fade-up" data-delay="300" src="img/shark.png"
                alt="The Global Shark Movement Project">
            <p class="reveal blur-fade-down" data-delay="1300"><span>Moving towards a step change in marine
                    animal tracking for conservation</span></p>
        </div>
        <div class="logos">
            <p class="logo-intro reveal blur-enter-left" data-delay="700">LED BY</p>
            <img src="img/mba-225x225.png" alt="" class="reveal blur-enter-left" data-delay="1000">
            <img src="img/cibio-1-208x122.png" alt="" class="reveal blur-enter-left" data-delay="1300">
            <img src="img/southampton-300x65.png" alt="" class="reveal blur-enter-left" data-delay="1600">
        </div>
    </div>
</section>