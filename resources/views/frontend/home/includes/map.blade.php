<section class="map">
    <div class="map-wrapper">
        <div class="container">
            <div class="row">
                <div class="how-box">
                    <div class="description-text">
                        <h2 class="reveal blur-fade-up" data-delay="200">How</h2>
                        <div class="reveal blur-fade-up" data-delay="400">
                            <p>GSMP brings together scientists and the shark tracking data they collect within a single
                                database to enable novel research in shark behaviour, ecology, conservation and
                                fisheries
                                science.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="img/map.png" class="map" alt="map" data-delay="500">
        <div class="map-pins">
            <div class="pin pin-1 reveal fade-anim-default" data-delay="600" style="top:34%; left:37%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/N-Atl_SFMako_JS-D-150x150.jpg" alt=""></div>
                    <p class="description">GSMP has tagged sharks right across the North Atlantic and has tracked them
                        over huge distances. More than 150 blue sharks, 130 tiger and 120 shortfin makos have been
                        tracked
                        here since 2002. The fastest swimming shark, the shortfin mako, moves up to 60 km a day. GSMP
                        data
                        have identified shark spatial hotspots and revealed that they are overlapped significantly by
                        longline fishing activity – sharks here are being overfished and have very little management to
                        control catches. However, GSMP analyses can help identify spatially where conservation should be
                        focused. [Read more in Projects and Publications]</p>
                </div>
            </div>
            <div class="pin pin-2 reveal fade-anim-default" data-delay="700" style="top:37%; left:28%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/NW-Atl_WhiteShark_JS-D-150x150.jpg" alt="">
                    </div>
                    <p class="description">GSMP researchers have spent years studying shark movements from U.S. and
                        Canadian waters, including great white sharks. A famous white shark called ‘Lydia’ tagged in
                        U.S.
                        waters headed west and crossed the Mid-Atlantic Ridge before turning round and heading back.
                        Protected in U.S. waters after decades of exploitation, white sharks there appear to be
                        increasing
                        in abundance.</p>
                </div>
            </div>
            <div class="pin pin-3 reveal fade-anim-default" data-delay="800" style="top:53%; left:28%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/C-Atl_OceanicWhitetip_NH-150x150.jpg" alt="">
                    </div>
                    <p class="description">GSMP is tagging threatened sharks throughout Florida waters and the Caribbean
                        Sea. The oceanic whitetip shark is ‘Critically Endangered’ in the western central Atlantic with
                        the
                        population at a fraction of its former biomass. GSMP researchers have tracked its space use,
                        finding
                        that some Caribbean marine reserves banning industrial fishing may protect a significant
                        proportion
                        of habitats.</p>
                </div>
            </div>
            <div class="pin pin-4 reveal fade-anim-default" data-delay="900" style="top:54%; left:39%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/CW-Atl_Blueshark_KH-150x150.jpg" alt=""></div>
                    <p class="description">Blue and shortfin mako sharks are being tracked by GSMP here because these
                        sharks are faced with a double whammy: climate-driven ocean deoxygenation that leads to oxygen
                        minimum zone (OMZ) expansion and shark habitat compression coupled with high fishing intensity
                        above
                        the OMZ. These combined effects of shark habitat compression and high fishing effort likely make
                        pelagic sharks here more susceptible to surface longline fisheries than adjacent waters.
                        Therefore,
                        this may be a region requiring special measures to mitigate climate and fishing effects on
                        sharks.
                        [Read more in Projects]</p>
                </div>
            </div>
            <div class="pin pin-5 reveal fade-anim-default" data-delay="1000" style="top:62%; left:36%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/C-Atl_Tigershark_NH-150x150.jpg" alt=""></div>
                    <p class="description">GSMP scientists are following the movements of whale, tiger and oceanic
                        whitetip sharks in tropical waters off Brazil to find out how changing environments influence
                        movement patterns and spatial distributions. Key questions concern where sharks migrate to and
                        where
                        and when they interact with longline and purse seine fisheries.</p>
                </div>
            </div>
            <div class="pin pin-6 reveal fade-anim-default" data-delay="1100" style="top:69%; left:42%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/Tagged-tiger_NH-150x150.jpg" alt=""></div>
                    <p class="description">GSMP researchers stationed at this remote island have been tagging tiger and
                        Galapagos sharks to find out whether they remain close to oceanic islands for much of the time,
                        or
                        are just passing through on long-distance migrations spanning the South Atlantic. Determining if
                        space use of some pelagic shark species is relatively localised around remote islands may help
                        devise management measures to better protect them in these areas.</p>
                </div>
            </div>
            <div class="pin pin-7 reveal fade-anim-default" data-delay="1200" style="top:85%; left:39%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <p class="description">GSMP has found few satellite trackings of pelagic sharks have been undertaken
                        in the South Atlantic, which identifies a real knowledge gap. This is of concern because
                        longline
                        fishing effort is high throughout the high seas of the South Atlantic.</p>
                </div>
            </div>
            <div class="pin pin-8 reveal fade-anim-default" data-delay="1300" style="top:87%; left:55%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/White-shark_NH-150x150.jpg" alt=""></div>
                    <p class="description">White sharks and bull sharks are being tracked by GSMP teams off South Africa
                        to better understand their use of coastal and oceanic waters. White sharks are protected in
                        South
                        African waters so it is important to know where they go in relation to people using the sea, but
                        also to determine whether they are at risk from fisheries capture by occurring in the fishing
                        areas
                        at the same time as fishers.</p>
                </div>
            </div>
            <div class="pin pin-9 reveal fade-anim-default" data-delay="1400" style="top:76%; left:58.5%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/W-Ind_Bull_JSEL-150x150.jpg" alt=""></div>
                    <p class="description">GSMP scientists have been tagging and tracking blue, tiger, whale, white,
                        silky
                        and bull sharks in the rich, deep waters between southeast Africa and Madagascar. The high
                        productivity at depth may be one reason why whale sharks here have been tracked diving down to
                        over
                        1 km depth.</p>
                </div>
            </div>
            <div class="pin pin-10 reveal fade-anim-default" data-delay="1500" style="top:66%; left:63%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/W-Ind_Silky_JSEL-150x150.jpg" alt=""></div>
                    <p class="description">Silky, whale and bull sharks are being tracked in the Seychelles. It is
                        thought
                        the impacts of fisheries on Indian Ocean sharks have been as severe as those documented in the
                        Atlantic and Pacific, however knowledge about where Indian Ocean sharks and fisheries interact
                        is
                        largely missing. Despite bull sharks being thought to remain in shallow coastal waters, GSMP
                        researchers have discovered a pregnant bull shark migrated across open ocean from the Seychelles
                        to
                        Madagascar and back, a 4,000 km round trip, highlighting the need for international
                        collaboration to
                        manage the regional population.</p>
                </div>
            </div>
            <div class="pin pin-11 reveal fade-anim-default" data-delay="1600" style="top:79%; left:79%">
                <div class="circle"></div>
                <div class="map-tooltip p-left">
                    <div class="pin-image">
                        <img src="img/SFMako-150x150.jpg" alt=""></div>
                    <p class="description">White sharks have been tracked migrating up the west coast of Australia, from
                        the southwest tip to Ningaloo Reef in the north during October and November. Shortfin makos have
                        also been tracked here by GSMP scientists, which raises the question whether both species
                        converge
                        there to feed on fishes moving from shelf to oceanic waters seasonally.</p>
                </div>
            </div>
            <div class="pin pin-12 reveal fade-anim-default" data-delay="1700" style="top:88%; left:87%">
                <div class="circle"></div>
                <div class="map-tooltip p-left">
                    <div class="pin-image">
                        <img src="img/NW-Atl_WhiteShark_JS-D-150x150.jpg" alt="">
                    </div>
                    <p class="description">Blue, shortfin mako and white sharks are tracked by GSMP researchers off
                        southern Australia. Understanding the movements of blue sharks and shortfin makos is important
                        to
                        inform management, since these species are globally the most commonly caught pelagic shark by
                        longline fisheries. White sharks in this region, as off South Africa and Californian, feed on
                        seals
                        seasonally resident at colonies, so knowing their movements can help to find out if sharks are
                        similarly resident seasonally and where they go to after the seals disperse to sea.</p>
                </div>
            </div>
            <div class="pin pin-13 reveal fade-anim-default" data-delay="1800" style="top:81%; left:94%">
                <div class="circle"></div>
                <div class="map-tooltip p-left">
                    <div class="pin-image">
                        <img src="img/image1-150x150.jpg" alt=""></div>
                    <p class="description">Pelagic shark movements here are influenced by the rich waters of the East
                        Australia Current and those of the Great Barrier Reef. Blue, shortfin mako, tiger and white
                        sharks
                        are being tracked by GSMP. Coastal and shelf movements of tiger sharks are common in this region
                        and
                        the large scale movements they make away from relatively small protected areas (e.g. Australian
                        Commonwealth Marine Reserves and coastal barrier reef marine protected areas) expose them to
                        fisheries. GSMP is studying the spatial dynamics of sharks and fishing vessels.</p>
                </div>
            </div>
            <div class="pin pin-14 reveal fade-anim-default" data-delay="1900" style="top:84%; left:100%">
                <div class="circle"></div>
                <div class="map-tooltip p-left">
                    <div class="pin-image">
                        <img src="img/image6-150x150.jpg" alt=""></div>
                    <p class="description">The warm-bodied porbeagle and white sharks are being studied by GSMP
                        researchers. The porbeagle shark is assessed as globally ‘Endangered’ and is managed by strict
                        quotas in New Zealand waters. It is necessary to understand its movements, preferred habitats
                        and
                        spatial overlap with fisheries because when caught as bycatch on longlines some 40% of
                        individuals
                        are already dead. GSMP tracking studies of sharks and longliners can help identify where most
                        bycatch is occurring, and inform bycatch mitigation to protect the population given the high
                        mortality from discarding.</p>
                </div>
            </div>
            <div class="pin pin-15 reveal fade-anim-default" data-delay="2000" style="top:54%; left:87%">
                <div class="circle"></div>
                <div class="map-tooltip p-left">
                    <div class="pin-image">
                        <img src="img/WhaleSharkSpots-150x150.jpg" alt=""></div>
                    <p class="description">Whale sharks have been tracked by GSMP teams in this region, but generally
                        very
                        little is known about the movements of other pelagic shark species here. This is a region where
                        GSMP
                        aims to focus more attention.</p>
                </div>
            </div>
            <div class="pin pin-16 reveal fade-anim-default" data-delay="2100" style="top:43%; left:90%">
                <div class="circle"></div>
                <div class="map-tooltip p-left">
                    <div class="pin-image">
                        <img src="img/WC-Atl_TaggedHammer_NH-150x150.jpg" alt="">
                    </div>
                    <p class="description">GSMP scientists have begun to track sharks in this region including Galapagos
                        sharks. However, this region represents a major knowledge gap for pelagic shark movements and
                        behaviour because seemingly few have been tracked here. This region needs to be a focus of
                        collaborative shark science to reveal where sharks go and what they are doing in relation to
                        known
                        environmental changes and anthropogenic impacts already occurring.</p>
                </div>
            </div>
            <div class="pin pin-17 reveal fade-anim-default" data-delay="2200" style="top:40%; left:5%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/image5-150x150.jpg" alt=""></div>
                    <p class="description">The Northeast Pacific is one of the most intensively studied regions with
                        respect to pelagic sharks. Between 2000 and 2010 several hundred pelagic sharks including white,
                        salmon, blue and shortfin makos were tracked as part of the Tracking of Pacific Pelagics (TOPP)
                        project. GSMP is fortunate to be able to link up with this research team to incorporate their
                        freely
                        available data into global analyses.</p>
                </div>
            </div>
            <div class="pin pin-18 reveal fade-anim-default" data-delay="2300" style="top:62%; left:16%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/C-Atl_Tigershark_NH-150x150.jpg" alt=""></div>
                    <p class="description">GSMP researchers working in the Galapagos Islands and other islands in the
                        region already work together very closely within the MigraMar project. Tiger, whale and
                        hammerhead
                        sharks are being tracked to understand their basic ecology in this incredibly productive part of
                        the
                        Pacific ocean, and how their space use overlaps with threats such as longline and purse seine
                        fishing. Illegal, unreported and unregulated (IUU) fisheries are problem in this region so the
                        scientists are also quantifying the spatial protection that can be achieved within marine
                        reserves
                        given the necessary enforcement.</p>
                </div>
            </div>
            <div class="pin pin-19 reveal fade-anim-default" data-delay="2400" style="top:82%; left:19%">
                <div class="circle"></div>
                <div class="map-tooltip p-right">
                    <div class="pin-image">
                        <img src="img/SFMako-150x150.jpg" alt=""></div>
                    <p class="description">Shortfin mako sharks are being tagged and tracked to determine space use
                        patterns and diving behaviour. Analyses show makos move throughout the region over huge
                        distances,
                        seemingly always on the move. They spend most time diving in mixed layer depth but occasionally
                        dive
                        down to 900 m.</p>
                </div>
            </div>
        </div>
    </div>
</section>
