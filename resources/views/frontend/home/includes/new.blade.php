<section class="news">
    <div class="container">
        <h2 class="reveal blur-fade-up">NEWS</h2>
        <div class="news-slider">
            <div class="row">
                <div class="col-6">
                    <div>
                        <article class="news-item reveal blur-enter-left" data-delay="400">
                            <a href="" class="news-link" tabindex="0">
                                <div class="news-content-wrapper">
                                    <h2>What are the most dangerous places for sharks?</h2>
                                    <time datetime="2021-03-02 14:03">02/03/21</time>
                                    <div class="overlay"></div>
                                    <img width="378" height="378" src="img/White-shark_NH-378x378.jpg"
                                        class="obj-fit-cover wp-post-image" alt="" loading="lazy">
                                </div>
                            </a></article>
                    </div>
                </div>
                <div class="col-6">
                    <div>
                        <article class="news-item reveal blur-enter-left" data-delay="500">
                            <a href="" class="news-link" tabindex="0">
                                <div class="news-content-wrapper">
                                    <h2>Mako sharks speeding to the brink?</h2>
                                    <time datetime="2021-01-23 14:01">23/01/21</time>
                                    <div class="overlay"></div>
                                    <img width="378" height="378" src="img/SFMako-378x378.jpg"
                                        class="obj-fit-cover wp-post-image" alt="" loading="lazy">
                                </div>
                            </a></article>
                    </div>
                </div>
            </div>
        </div>

        <a href="" class="all-news">View all news</a>
    </div>
</section>