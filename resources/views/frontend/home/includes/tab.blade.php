<section class="tabs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="filter-wrapper">
                    <ul class="tabs-picker cf" data-method="tabPicker">
                        <li class="reveal blur-fade-down active" data-delay="100">
                            <h2 class="no-barba tab-changer" data-id="1">Science</h2>
                        </li>
                        <li class="reveal blur-fade-down" data-delay="200">
                            <h2 class="no-barba tab-changer" data-id="2">Conservation</h2>
                        </li>
                        <li class="reveal blur-fade-down" data-delay="300">
                            <h2 class="no-barba tab-changer" data-id="3">Impact</h2>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12">
                <div class="content-wrapper reveal blur-fade-up" data-method="contentHeight" style="height: 847px;">
                    <div class="content active" data-id="1">
                        <p>GSMP tracks free-ranging sharks using sophisticated, miniature electronic tags that are
                            temporarily attached to them for logging and/or relaying data about their movements,
                            behaviour,
                            physiology and/or environment.</p>
                        <div class="terrain-wrapper">
                            <img src="img/shark(1).png" class="reveal blur-fade-up" data-delay="200" alt=""></div>
                        <div class="about-btn image-margin-fix">
                            <div class="about-animation">
                                <a href="" class="more-button">
                                    <p>more about</p>
                                    <svg class="spinner" width="110px" height="110px" viewBox="0 0 66 66"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <circle class="path" fill="transparent" stroke-width="1" cx="33" cy="33" r="30"
                                            stroke="url(#gradient2)"></circle>
                                        <lineargradient id="gradient2">
                                            <stop offset="50%" stop-color="#2dcbff" stop-opacity="1"></stop>
                                            <stop offset="65%" stop-color="#2dcbff" stop-opacity=".5"></stop>
                                            <stop offset="100%" stop-color="#2dcbff" stop-opacity="0"></stop>
                                        </lineargradient>
                                        <svg class="spinner-dot dot" width="5px" height="5px" viewBox="0 0 66 66"
                                            xmlns="http://www.w3.org/2000/svg" x="37" y="1.5">
                                            <circle class="path" fill="#2dcbff" cx="33" cy="33" r="30"></circle>
                                        </svg>
                                    </svg>
                                </a></div>
                        </div>
                    </div>
                    <div class="content " data-id="2">
                        <p>Understanding the complexities of shark movements and distributions with respect to changing
                            environment and anthropogenic threats is crucial for improving conservation.</p>
                        <div class="about-btn ">
                            <div class="about-animation">
                                <a href="" class="more-button">
                                    <p>more about</p>
                                    <svg class="spinner" width="110px" height="110px" viewBox="0 0 66 66"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <circle class="path" fill="transparent" stroke-width="1" cx="33" cy="33" r="30"
                                            stroke="url(#gradient2)"></circle>
                                        <lineargradient id="gradient2">
                                            <stop offset="50%" stop-color="#2dcbff" stop-opacity="1"></stop>
                                            <stop offset="65%" stop-color="#2dcbff" stop-opacity=".5"></stop>
                                            <stop offset="100%" stop-color="#2dcbff" stop-opacity="0"></stop>
                                        </lineargradient>
                                        <svg class="spinner-dot dot" width="5px" height="5px" viewBox="0 0 66 66"
                                            xmlns="http://www.w3.org/2000/svg" x="37" y="1.5">
                                            <circle class="path" fill="#2dcbff" cx="33" cy="33" r="30"></circle>
                                        </svg>
                                    </svg>
                                </a></div>
                        </div>
                    </div>
                    <div class="content " data-id="3">
                        <p>GSMP promotes the use of its scientific results to inform policy relating to shark
                            conservation
                            and the sustainable management of populations.</p>
                        <p>GSMP research teams engage actively with regulatory authorities and policy makers to
                            communicate
                            new science that can be used to improve shark conservation.</p>
                        <div class="about-btn ">
                            <div class="about-animation">
                                <a href="" class="more-button">
                                    <p>more about</p>
                                    <svg class="spinner" width="110px" height="110px" viewBox="0 0 66 66"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <circle class="path" fill="transparent" stroke-width="1" cx="33" cy="33" r="30"
                                            stroke="url(#gradient2)"></circle>
                                        <lineargradient id="gradient2">
                                            <stop offset="50%" stop-color="#2dcbff" stop-opacity="1"></stop>
                                            <stop offset="65%" stop-color="#2dcbff" stop-opacity=".5"></stop>
                                            <stop offset="100%" stop-color="#2dcbff" stop-opacity="0"></stop>
                                        </lineargradient>
                                        <svg class="spinner-dot dot" width="5px" height="5px" viewBox="0 0 66 66"
                                            xmlns="http://www.w3.org/2000/svg" x="37" y="1.5">
                                            <circle class="path" fill="#2dcbff" cx="33" cy="33" r="30"></circle>
                                        </svg>
                                    </svg>
                                </a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>