<section class="tweets">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="reveal blur-fade-up">TWEETS</h2>
                <div class="tweets-wrapper">
                    <div class="row">
                        <div class="col-4">
                            <div class="tweet-item reveal blur-enter-right" data-delay="100">
                                <div class="profile-img"><img src="img/oNYU10HH_normal.jpg" class="obj-fit-cover"
                                        alt="TheSimsLab"></div>
                                <div class="tweet-text">
                                    <p>Interesting &amp; gd to see - off SW England &amp; off Newfoundland juveniles
                                        make up 2.8%
                                        &amp; 2.6% of all annual sightings respectively - so hopefully there's a few
                                        more too <a href="" target="_blank">https://t.co/2hlXXqaZW0</a></p>
                                    <div class="time">2 days ago</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="tweet-item reveal blur-enter-right" data-delay="200">
                                <div class="profile-img"><img src="img/oNYU10HH_normal.jpg" class="obj-fit-cover"
                                        alt="TheSimsLab"></div>
                                <div class="tweet-text">
                                    <p>You may see it again - we satellite tagged a 2-3m <a href=""
                                            target="_blank">#baskingshark</a> in
                                        July 2001 in Scotland, tracked it for 229 days before tag popped up off N Wales
                                        in winter -
                                        so it didn't go that far really in that time
                                        <a href="" target="_blank">https://t.co/Lz3p0Tn0Lu</a> <a href=""
                                            target="_blank">https://t.co/45EPNoF2uN</a></p>
                                    <div class="time">3 days ago</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="tweet-item reveal blur-enter-right" data-delay="300">
                                <div class="profile-img"><img src="img/oNYU10HH_normal.jpg" class="obj-fit-cover"
                                        alt="TheSimsLab"></div>
                                <div class="tweet-text">
                                    <p>Awesome!! Juvenile 2-3 m <a href="" target="_blank">#baskingsharks</a> have been
                                        observed
                                        consistently off W Scotland, Ireland
                                        &amp; SW England over past decades, often w/ adults – e.g. 93 sharks caught off
                                        Scotland in
                                        1950s ranged from 1.7 - 9.5 m total length - UK/IRE breeding ground?
                                        <a href="" target="_blank">https://t.co/poHWmiWOsk</a> <a href=""
                                            target="_blank">https://t.co/45EPNoF2uN</a></p>
                                    <div class="time">3 days ago</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>