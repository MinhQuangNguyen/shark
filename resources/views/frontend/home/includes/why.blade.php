<section class="why">
    <div class="container">
        <div class="row">
            <div class="col-4 description-text">
                <h2 class="reveal blur-fade-up" data-delay="100">Why</h2>
                <div class="reveal blur-fade-up" data-delay="400">
                    <p>Tens of millions of pelagic sharks are harvested each year by high seas fisheries but with little
                        or no management for the majority of species. Sharks are particularly susceptible to the effects
                        of
                        human exploitation due to slow growth rates, late age at maturity and low fecundity, traits
                        making
                        them comparable to marine mammals in terms of vulnerability. Global data on pelagic shark
                        habitat
                        hotspots, spatial patterns of vulnerability in relation to fisheries, and how sharks respond to
                        changing environment are lacking, precluding understanding where in the global ocean
                        conservation
                        needs to be focused.</p>
                </div>
            </div>
            <div class="col-8 image-wrapper reveal blur-fade-down">
                <h3 class="reveal blur-fade-down" data-delay="600">Many shark populations are declining</h3>
                <img src="img/boat-728x619.jpg" class="obj-fit-cover" alt="">
            </div>
        </div>
    </div>
</section>