@extends('frontend.layouts.khangchi')

@section('title', __('Home'))

@section('content')
    @include('frontend.home.includes.intro')
    @include('frontend.home.includes.about')
    @include('frontend.home.includes.map')
    @include('frontend.home.includes.why')
    @include('frontend.home.includes.tab')
    @include('frontend.home.includes.new')
    @include('frontend.home.includes.tweets')
@endsection
