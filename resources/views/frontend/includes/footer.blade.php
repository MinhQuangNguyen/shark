<div class="partners">
  <div class="container">
      <div class="row">
          <div class="col-4 footer-col">
              <p>Led by</p>
              <ul class="logos">
                  <li>
                      <a href="" target="_blank">
                          <img src="img/mba-225x225.png" alt="">
                      </a>
                  </li>
                  <li>
                      <a href="" target="_blank">
                          <img src="img/cibio-1-208x122.png" alt="">
                      </a>
                  </li>
                  <li>
                      <a href="" target="_blank">
                          <img src="img/southampton-300x65.png" alt="">
                      </a>
                  </li>
              </ul>
          </div>
          <div class="col-4 footer-col">
              <p>Funded by</p>
              <ul class="logos">
                  <li>
                      <a href="" target="_blank">
                          <img src="img/mba-225x225.png" alt="">
                      </a>
                  </li>
                  <li>
                      <a href="" target="_blank">
                          <img src="img/cibio-1-208x122.png" alt="">
                      </a>
                  </li>
                  <li>
                      <a href="" target="_blank">
                          <img src="img/nerc-1-300x209.png" alt="">
                      </a>
                  </li>
                  <li>
                      <a href="" target="_blank">
                          <img src="img/0kOHUH-M-300x63.png" alt="">
                      </a>
                  </li>
              </ul>
          </div>
          <div class="col-4 footer-col">
              <p>FIND US</p>
              <ul class="social">
                  <li>
                      <a href="" target="_blank">
                          <svg class="twitter">
                              <use xlink:href="#twitter"></use>
                          </svg>
                          <span>Twitter</span>
                      </a>
                  </li>
              </ul>
          </div>
      </div>
  </div>
</div>

<footer>
    <div class="container">
      <div class="row">
        <div class="col-6 logo">
          <a href="" class="footer-logo" title="Global Shark Movement">
            <img src="img/logo.svg" alt="Global Shark Movement">
          </a>
          <p>Copyright © 1996-2021 <strong>Global Shark Movement</strong></p>
        </div>
        <div class="col-6 agency">
          <a href="" target="_blank">TASON</a></div>
      </div>
    </div>
  </footer>