<div class="lang">
    <div class="dropdown">
        <a class="nav-link-lang" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <div class="lang-text">
                {{app()->getLocale() == 'vi'? 'VI': 'EN'}}
            </div>
            <div class="lang-flag">

                <img src="{{asset('img/'.app()->getLocale().'.svg')}}" />
            </div>
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="lang/vi">
                <img src="{{asset('img/vi.svg')}}" /> {{__('Vietnamese')}}
            </a>
            <a class="dropdown-item" href="lang/en">
                <img src="{{asset('img/en.svg')}}" /> {{ __('English') }}
            </a>
        </div>
    </div>
</div>
<div class="search">
    <div class="icon">
        <?php $lang = (app()->getLocale() === 'vi' ? 'tim-kiem' : 'search') ?>
        <form class="searchForm" action="{{ '/'.$lang }}" method="get">
            <div class="input-group">
                <input type="text" class="form-control search-inp" placeholder="{{ __('Search') }}..."
                    aria-describedby="basic-addon2" name="k" value="{{ request()->get('k') }}">
                <div class="input-group-append search-btn">
                    <i class="fas fa-search"></i>
                </div>
            </div>
        </form>
    </div>
</div>
