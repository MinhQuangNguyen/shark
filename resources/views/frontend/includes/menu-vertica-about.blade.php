<div class="menu-vertical">
    <div class="indicator"></div>
    <ul class="menu-list">
        @foreach ($menu as $menuItem)
            <li class="menu-item {{ $menuItem->isActive ? 'active' : '' }}" 
                data-id="{{ $menuItem->id }}" 
                data-url="{{ $menuItem->link }}" 
                data-title="{{__('Introduce')}} - {{ $menuItem->name }}"
            >
                <a href="javascript:void(0);">{{ $menuItem->name }}</a>
            </li>
        @endforeach
    </ul>
</div>
