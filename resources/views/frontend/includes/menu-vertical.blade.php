<div class="menu-vertical">
    <div class="indicator"></div>
    <ul class="menu-list">
        @foreach($newCategory as $key => $item)
        <li class="menu-item {{ $key === 0 ? 'active' : ''}}" x-on:click="onChange({{$item}})">
            <a href="javascript:void(0);">{{ $item->name }}</a>
        </li>
        @endforeach
    </ul>
</div>
