<div class="mobile-menu-wrapper">
    <div class="site-logo-mobile">
        <a href="" title="Global Shark Movement">
            <img src="img/logo.svg" alt="Global Shark Movement">
        </a></div>
    <div id="showMenu">
        <div class="nav-icon">
            <span></span>
            <span></span>
            <span></span></div>
    </div>
    <nav class="outer-nav right vertical">
        <ul class="main-menu">
            <li id="menu-item-7"
                class="scroll menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-7">
                <a href="#about" aria-current="page">About</a></li>
            <li id="menu-item-96" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96"><a
                    href="">Research</a></li>
            <li id="menu-item-344" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-344"><a
                    href="">People</a></li>
            <li id="menu-item-97" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-97"><a
                    href="">Publications</a></li>
            <li id="menu-item-245" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-245"><a
                    href="">News</a></li>
            <li id="menu-item-95" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-95"><a
                    href="">Contact</a></li>
        </ul>
    </nav>
</div>
<header id="page-header">
    <div class="site-logo reveal fade-anim-default" data-delay="400">
        <a href="" title="Global Shark Movement">
            <img src="img/logo.svg" alt="Global Shark Movement">
        </a></div>
    <div class="right-wrapper reveal fade-anim-default" data-delay="1200">
        <nav>
            <ul class="main-menu"> 
                <li
                    class="scroll menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-7">
                    <a href="#about" aria-current="page">About</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96"><a
                        href="">Research</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-344"><a href="">People</a>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-97"><a
                        href="">Publications</a>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-245"><a href="">News</a>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-95"><a href="">Contact</a>
                </li>
            </ul>
        </nav>
    </div>
</header>
