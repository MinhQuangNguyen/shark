<div class="btn-kc rhombus-date">
    <div class="btn-content">
        <div class="day">{{ $day }}</div>
        <div class="month">th {{ $month }}</div>
    </div>
</div>
