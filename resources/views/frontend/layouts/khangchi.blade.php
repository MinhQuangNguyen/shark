<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ appName() }} | @yield('title')</title>
    <meta name="description" content="@yield('meta_description', appName())">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">

    <meta property="og:locale" content="{{ htmlLang() }}" />
    <meta property="og:title" content="@yield('title', appName())" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ request()->fullUrl() }}" />
    <meta property="og:image" content="@yield('image', asset('/img/imgAbout.png'))">
    <meta property="og:description" content="@yield('description', '')" />
    <meta property="og:site_name" content="{{ appURL() }}" />
    @yield('meta')

    @stack('before-styles')
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">
    <livewire:styles />
    @stack('after-styles')

</head>

<body>

    @include('frontend.includes.header', ['menu' => $menu])
    <div id="page-wrapper"> 
        {{-- @include('includes.partials.messages') --}}
        <div id="barba-wrapper" aria-live="polite" class="">
            <div class="page-template-default page page-id-89 barba-container singular fadeIn" data-namespace="page"style="visibility: visible; opacity: 1;">
              @yield('content')
              @include('frontend.includes.footer')
            </div>
          </div>
    </div>
    <!--app-->

    @stack('before-scripts')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/frontend.js') }}"></script>
 
    <livewire:scripts />
    @include('frontend.includes.js')
    {{-- <script src="js/jquery.min.js"></script> --}}
    <script src="js/f04d6.js"></script>
    @stack('after-scripts')
</body>

</html>
