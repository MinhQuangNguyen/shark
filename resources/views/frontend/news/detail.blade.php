@extends('frontend.layouts.khangchi')

@section('title', __('News'))

@section('content')
    @include('frontend.news.includes.slide', $slides)

    @if($post->category->type === 2)
        @include('frontend.news.includes.detailLibrary')
    @else
        @include('frontend.news.includes.detailBlog')
    @endif
@endsection
