@extends('frontend.layouts.khangchi')

@section('title', __('News'))
@section('content')
    @include('frontend.news.includes.slide')
    <div class="detail-blog-main">
        <div class="container">
            <div class="row justify-content-around justify-content-lg-between">
                <div class="col-14 col-lg-9">
                    <div class="text">Nhân kỷ niệm 90 năm ngày Phụ Nữ Việt Nam (20/10/1930 – 20/10/2020), Công ty Cổ phần Tập đoàn HASCO (Tập đoàn HASCO) đã tổ chức buổi lễ tri ân, tôn vinh những đóng góp to lớn của tập thể nữ cán bộ nhân viên (CBNV) Tập đoàn.</div>
                    <div class="img-blog">
                        <img src="{{asset('img/img21.svg')}}" alt="">
                        <div class="desc-img">Đại diện Ban Lãnh đạo trao tặng bó hoa tươi thắm đến toàn thể CBNV nữ Tập đoàn HASCO</div>
                    </div>
                    <div class="text">Nhân kỷ niệm 90 năm ngày Phụ Nữ Việt Nam (20/10/1930 – 20/10/2020), Công ty Cổ phần Tập đoàn HASCO (Tập đoàn HASCO) đã tổ chức buổi lễ tri ân, tôn vinh những đóng góp to lớn của tập thể nữ cán bộ nhân viên (CBNV) Tập đoàn.</div>
                    <div class="img-blog">
                        <img src="{{asset('img/img21.svg')}}" alt="">
                    </div>
                </div>
                <div class="col-14 col-lg-4">
                    <div class="item-suggestions-new-feed">
                        <div class="title-suggerstions">Tin liên quan</div>
                        <div class="item-suggestions">
                            <a href="">Tập đoàn Khang Chi khai trương Trụ sở mới Văn phòng đại diện phía Nam tại Thành phố Hồ Chí Minh</a>
                            <div class="text">28/09/2020</div>
                        </div>
                        <div class="item-suggestions">
                            <a href="">Thế hệ trẻ Khang Chi với hoạt động tri ân kỷ niệm ngày Thương binh liệt sĩ</a>
                            <div class="text">28/09/2020</div>
                        </div>
                        <div class="item-suggestions">
                            <a href="">Gala Dinner “Chung Một Mái Nhà” Tập đoàn HASCO 2020</a>
                            <div class="text">28/09/2020</div>
                        </div>
                        <div class="item-suggestions">
                            <a href="">Chúc mừng ngày báo chí cách mạng Việt Nam</a>
                            <div class="text">28/09/2020</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
