@extends('frontend.layouts.khangchi')

@section('title', __('News'))
@section('content')
@include('frontend.news.includes.slide')
<div class="detail-blog-main">
    <div class="container">
        <div class="row justify-content-around justify-content-lg-between">
            <div class="col-14 col-lg-9">
                <div class="text">Nhân kỷ niệm 90 năm ngày Phụ Nữ Việt Nam (20/10/1930 – 20/10/2020), Công ty Cổ phần
                    Tập đoàn HASCO (Tập đoàn HASCO) đã tổ chức buổi lễ tri ân, tôn vinh những đóng góp to lớn của tập
                    thể nữ cán bộ nhân viên (CBNV) Tập đoàn.</div>
                <div class="img-blog">
                    <img src="{{asset('img/img21.svg')}}" alt="">
                    <div class="desc-img">Đại diện Ban Lãnh đạo trao tặng bó hoa tươi thắm đến toàn thể CBNV nữ Tập đoàn
                        HASCO</div>
                </div>
                <div class="text">Nhân kỷ niệm 90 năm ngày Phụ Nữ Việt Nam (20/10/1930 – 20/10/2020), Công ty Cổ phần
                    Tập đoàn HASCO (Tập đoàn HASCO) đã tổ chức buổi lễ tri ân, tôn vinh những đóng góp to lớn của tập
                    thể nữ cán bộ nhân viên (CBNV) Tập đoàn.</div>
                <div class="img-blog">
                    <img src="{{asset('img/img21.svg')}}" alt="">
                </div>
            </div>

            <div class="col-14 col-lg-4">
                <div class="listen-news-banner-main detail-library-img">
                    <div class="title-relate-library">Hình ảnh liên quan</div>

                    <div class="item-listen">
                        <div class="row no-gutters">
                            <a href="">
                                <div class="col-14 col-lg-14">
                                    <div class="background-image"
                                        style="background-image: url('{{ asset('img/listen1.svg') }}');">
                                        @include('frontend.includes.rhombus-date', ['day' => 27, 'month' => 12])
                                    </div>
                                </div>
                                <div class="col-14 col-lg-14">
                                    <div class="row full-height justify-content-end justify-content-end">
                                        <div class="col-11 col-lg-14">
                                            <div class="item-info-text-listen">
                                                <div class="title">Tập đoàn Khang Chi tôn vinh ngày Phụ Nữ Việt Nam
                                                    20/10</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="item-listen">
                        <div class="row no-gutters">
                            <a href="">
                                <div class="col-14 col-lg-14">
                                    <div class="background-image"
                                        style="background-image: url('{{ asset('img/listen1.svg') }}');">
                                        @include('frontend.includes.rhombus-date', ['day' => 27, 'month' => 12])
                                    </div>
                                </div>
                                <div class="col-14 col-lg-14">
                                    <div class="row full-height justify-content-end justify-content-end">
                                        <div class="col-11 col-lg-14">
                                            <div class="item-info-text-listen">
                                                <div class="title">Tập đoàn Khang Chi tôn vinh ngày Phụ Nữ Việt Nam
                                                    20/10
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="item-listen">
                        <div class="row no-gutters">
                            <a href="">
                                <div class="col-14 col-lg-14">
                                    <div class="background-image"
                                        style="background-image: url('{{ asset('img/listen1.svg') }}');">
                                        @include('frontend.includes.rhombus-date', ['day' => 27, 'month' => 12])
                                    </div>
                                </div>
                                <div class="col-14 col-lg-14">
                                    <div class="row full-height justify-content-end justify-content-end">
                                        <div class="col-11 col-lg-14">
                                            <div class="item-info-text-listen">
                                                <div class="title">Tập đoàn Khang Chi tôn vinh ngày Phụ Nữ Việt Nam
                                                    20/10</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
