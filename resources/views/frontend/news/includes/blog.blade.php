<div class="listen-news-banner-main">
    <div class="background-listen-news"></div>
    <div class="container">
        <div class="row">
            @if(isset($posts))
            @foreach($posts as $post)
            <div class="col-14 col-lg-7">
                <a href="{{ $post->category->slug.'/'.$post->slug.'.html' }}">
                    <div class="item-listen">
                        <div class="row no-gutters">
                            <div class="col-14 col-lg-6">
                                <div class="background-image"
                                    style="background-image: url('{{ asset($post->image) }}');"
                                >
                                    @include('frontend.includes.rhombus-date', ['day' => date_format($post->created_at, 'd'), 'month' => date_format($post->created_at, 'm')])
                                </div>
                            </div>
                            <div class="col-14 col-lg-8">
                                <div class="row full-height">
                                    <div class="col-14 col-lg-14">
                                        <div class="item-info-text-listen">
                                            <div class="title">{{ $post->title }}</div>
                                            <div class="text">{{ $post->description_short }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
            @endif
        </div>

        {{ $posts->appends(request()->query())->links() }}
    </div>
</div>
