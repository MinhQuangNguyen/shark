<div class="detail-blog-main">
    <div class="container">
        <div class="row justify-content-around justify-content-lg-between">
            <div class="col-14 col-lg-9">
                <div class="content">{!! $post->content !!}</div>
            </div>
            <div class="col-14 col-lg-4">
                <div class="item-suggestions-new-feed">
                    <div class="title-suggerstions">@lang('Related news')</div>
                    @if(isset($news_relates))
                    @foreach($news_relates as $key => $item)
                    <div class="item-suggestions">
                        <a href="{{ $item->category->getLink().'/'.$item->category->slug.'/'.$item->slug.'.html' }}">{{ $item->title }}</a>
                        <div class="text">{{ date_format($item->created_at, 'd/m/Y') }}</div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

