<div class="detail-blog-main">
    <div class="container">
        <div class="row justify-content-around justify-content-lg-between">
            <div class="col-14 col-lg-9">
                <div class="content">{!! $post->content !!}</div>
            </div>

            <div class="col-14 col-lg-4">
                <div class="listen-news-banner-main detail-library-img">
                    <div class="title-relate-library">{{ $post->type === 1 ? __('Related videos') : __('Related images') }}</div>
                    @if(isset($news_relates))
                    @foreach($news_relates as $key => $item)
                    <div class="item-listen">
                        <div class="row no-gutters">
                            <a href="{{ $item->category->getLink().'/'.$item->category->slug.'/'.$item->slug.'.html' }}">
                                <div class="col-14 col-lg-14">
                                    <div class="background-image"
                                        style="background-image: url('{{ asset('img/listen1.svg') }}');">
                                        @include('frontend.includes.rhombus-date', ['day' => date_format($item->created_at, 'd'), 'month' => date_format($item->created_at, 'm')])
                                    </div>
                                </div>
                                <div class="col-14 col-lg-14">
                                    <div class="row full-height justify-content-end justify-content-end">
                                        <div class="col-11 col-lg-14">
                                            <div class="item-info-text-listen">
                                                <div class="title">{{ $item->title }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
