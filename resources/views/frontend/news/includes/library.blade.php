<div class="item-library-mqn-main">
    <div class="background-white-mqn-news"></div>
    <div class="library-news-main">
        <div class="container">
            <div class="row" x-data="library()" x-init="init()">
                <div class="col-14 col-lg-4">
                    <div class="item-mennu-new-feed">
                        <div class="title">@lang('Library')</div>
                        @include('frontend.includes.menu-vertica-news')
                    </div>
                </div>
                <div class="col-14 col-lg-10">
                    <template x-if="news.length">
                    <div>
                        <div class="row">
                            <template x-for="(item, index) in news" :key="index">
                                <div class="col-14 col-lg-7">
                                    <a x-bind:href="type ? item.link : `${link}/${slug}/${item.slug}.html`" target="_blank">
                                        <div class="item-listen">
                                            <div class="row no-gutters">
                                                <div class="col-14 col-lg-14">
                                                    <div class="background-image"
                                                        :style="`background-image: url(${item.image})`">
                                                        <!-- @include('frontend.includes.rhombus-date', ['day' => 27, 'month' => 12]) -->
                                                        <div class="btn-kc rhombus-date">
                                                            <div class="btn-content">
                                                                <div class="day" x-text="getDay(item.created_at)"></div>
                                                                <div class="month" x-text="getMonth(item.created_at)">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-14 col-lg-14">
                                                    <div
                                                        class="row full-height justify-content-end justify-content-end">
                                                        <div class="col-10 col-lg-14">
                                                            <div class="item-info-text-listen">
                                                                <div class="title" x-text="item.title"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </template>
                        </div>

                        <div class="pagination-main">
                            <div class="row justify-content-center">
                                <div class="col-1-5 col-lg-2-3">
                                    <div class="btn-kc btn-back partner-prev disabled" x-on:click="previousPage()">
                                        <div class="btn-content"></div>
                                    </div>
                                </div>
                                <div class="col-1-5 col-lg-2-3">
                                    <div class="item-number-pagination">
                                        <span x-text="renderText()"></span>
                                    </div>
                                </div>
                                <div class="col-1-5 col-lg-2-3">
                                    <div class="btn-kc btn-next partner-next disabled" x-on:click="nextPage()">
                                        <div class="btn-content"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </template>
                </div>
            </div>
        </div>
    </div>
</div>

@push('after-scripts')
<script type="text/javascript">
    function library() {
        return {
            news: [],
            data: {},
            currentPage: 0,
            type: 0,
            totalPage: 0,
            slug: "{{ app()->getLocale() === 'vi' ? 'thu-vien' : 'library' }}",
            link: "{{ app()->getLocale() === 'vi' ? '/tin-tuc' : '/news' }}",
            onChange(type) {
                let news = [...(this.data[type] || [])]
                let newsT = news[0] ? news[0] : []
                this.currentPage = 0
                this.totalPage = news.length ? news.length : 0
                this.news = newsT
                this.type = type
            },
            previousPage() {
                let currentP = this.currentPage - 1
                if(currentP < 0) return
                this.news = this.data[this.type].length ? this.data[this.type][currentP] : []
                this.currentPage = currentP
            },
            nextPage() {
                let current = this.currentPage + 1
                if(current >= this.totalPage) return
                this.news = this.data[this.type].length ? this.data[this.type][current] : []
                this.currentPage = current
            },
            renderText() {
                let current = this.currentPage + 1
                return `${current < 10 ? `0${current}` : current }/${this.totalPage < 10 ? `0${this.totalPage}` : this.totalPage}`
            },
            convertDate(date) {
                return moment(date).format('DD/MM/YYYY')
            },
            getDay(date) {
                return moment(date).format('DD')
            },
            getMonth(date) {
                return `th ${moment(date).format('MM')}`
            },
            init() {
                let data = @json($libraries);
                let dataG = _.groupBy(data, 'type')
                let result = {}
                for(let index in dataG) {
                  result[index] = _.chunk(dataG[index], 6)
                }
                this.news = result[this.type].length ? result[this.type][this.currentPage] : []
                this.totalPage = result[this.type].length ? result[this.type].length : 0
                this.data = result
            },
        }
    }

</script>
@endpush('after-scripts')
