<?php $lang = (app()->getLocale() === 'vi' ? 'tin-tuc' : 'news') ?>
<div class="searchNews">
    <form class="searchForm">
        <div class="container">
            <div class="row">
                <div class="col-14">
                    <h4 class="title">@lang('News')</h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-14 col-lg-6">
                    <div class="search-news">
                        <input type="text" class="white-input search-inp" placeholder="{{ __('Search') }}" name="keyword" value="{{ request()->get('keyword') }}">
                        <div class="search-icon"><i class="fas fa-search"></i></div>
                    </div>
                </div>
                <div class="col-14 col-lg-4">
                    <select class="selectpicker white-select search-select post" data-style="red-select-btn">
                        <option value="{{ $lang }}">@lang('All')</option>
                        @if(isset($categories))
                        @foreach($categories as $category)
                            <option value="{{ $lang.'/'.$category->slug }}"
                                {{ request()->is($lang.'/'.$category->slug) ? 'selected' : ''}}
                            >
                                {{ $category->name }}
                            </option>
                        @endforeach
                    @endif
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
