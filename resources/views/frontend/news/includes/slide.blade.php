<!-- header slide -->
<div class="home-slide slide-kc slide-news">
    <div id="home-slide-main" class="carousel slide  w-95" data-ride="carousel">
        <ol class="carousel-indicators d-none d-sm-flex">
            @if(isset($slides) && count($slides) > 1)
            @foreach($slides as $key => $slide)
            <li data-target="#home-slide-main" data-slide-to="{{ $key }}" class="{{ $key === 0 ? 'active' : ''}}"></li>
            @endforeach
            @endif
        </ol>
        <div class="carousel-inner">
            @if(isset($slides))
            @foreach($slides as $key => $slide)
            <div class="carousel-item {{ $key === 0 ? 'active' : ''}}">
                <div class="img" style="background-image: url({{ asset($slide->image) }})"></div>
            </div>
            @endforeach
            @endif
        </div>
        @if(isset($slides) && count($slides) > 1)
        <a class="carousel-control-prev" href="#home-slide-main" role="button" data-slide="prev">
            <div class="btn-kc btn-back">
                <div class="btn-content"></div>
            </div>
        </a>
        <a class="carousel-control-next" href="#home-slide-main" role="button" data-slide="next">
            <div class="btn-kc btn-next">
                <div class="btn-content"></div>
            </div>
        </a>
        @endif
    </div>
    <div class="title-group">
        @if(isset($slides))
        @foreach($slides as $key => $slide)
        <div class="item padding-none {{ $key === 0 ? 'active' : ''}}">
            <div class="item-slide-news">
                <div class="tag">{{ $slide->title }}</div>
                <div class="title">{!! $slide->content !!}</div>
            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>
<!-- end header slide -->
