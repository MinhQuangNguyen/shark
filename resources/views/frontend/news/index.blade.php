@extends('frontend.layouts.khangchi')

@section('title', __('News'))

@section('content')
    @include('frontend.news.includes.slide', $slides)
    @include('frontend.news.includes.searchNew', $categories)
    @include('frontend.news.includes.blog', $posts)
    @include('frontend.news.includes.library', $libraries)
    {{-- tin tức --}}
@endsection
