<section class="subpage-content">
    <div class="container">
      <div class="row">
        <div class="title col-lg-12">
          <h1 class="reveal blur-fade-down">Research</h1>
        </div>
      </div>
      <div class="user-content">
        <div class="research">
          <div class="research">
            <div class="row ">
              <div class="col-xs-12 col-lg-8 research-image reveal blur-fade-down" data-delay="200">
                <div class="profile-image">
                  <img src="img/GSMP-Database-780x520.jpg" alt=""
                    class="obj-fit-cover blend-img"></div>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="col-xs-12 col-lg-8 research-data-wrapper reveal blur-fade-up align-self-end">
                <div class="person-data">
                  <div class="number">01</div>
                  <h2>GSMP Database</h2>
                  <div class="bio">
                    <p>The GSMP Database holds over 2,000 individual satellite tracks of large pelagic sharks from
                      23 species spanning over 280,000 track days.</p>
                  </div>
                  <div class="row">
                    <div class="hidden-content col-xs-12 col-lg-12">
                      <p>
                        There are also dive data for about a third of these shark tracks. For each daily location
                        along a shark track, 16 environmental variables have been extracted from global databases,
                        extending from the surface to hundreds of metres depth. The Database also holds fishing
                        vessel movements and fishing effort data derived from the Automatic Identification System
                        (AIS) and Vessel Monitoring System (VMS). The Database represents an important
                        international resource for the GSMP Team to analyse and understand shark movements,
                        behaviour and spatial distributions in relation to natural and anthropogenic impacts for
                        conservation and management outcomes.</p>
                    </div>
                  </div>
                  <div class="expand" data-method="expandBio"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="research">
            <div class="row justify-content-center">
              <div class="col-xs-12 col-lg-8 research-image reveal blur-fade-down" data-delay="200">
                <div class="profile-image">
                  <img
                    src="img/Global-overlap-of-sharks-and-fisheries-780x520.jpg"
                    alt="" class="obj-fit-cover blend-img"></div>
              </div>
            </div>
            <div class="row justify-content-end">
              <div class="col-xs-12 col-lg-8 research-data-wrapper reveal blur-fade-up align-self-end">
                <div class="person-data">
                  <div class="number">02</div>
                  <h2>Global overlap of sharks and fisheries</h2>
                  <div class="bio">
                    <p>Since fishing fleets expanded into the open ocean over the past 50 years oceanic pelagic
                      sharks have been subjected to levels of exploitation that have led to dramatic declines in
                      abundance of some species.</p>
                  </div>
                  <div class="row">
                    <div class="hidden-content col-xs-12 col-lg-12">
                      <p>
                        Knowing the migration routes, space use and changing distributions of sharks is crucial
                        for effective conservation and management. However, shark spatial dynamics are poorly
                        understood, particularly in relation to where fisheries exploitation actually occurs, and
                        despite there being global harvesting there are very few regulations to control shark
                        catches in the High Seas (areas beyond national jurisdiction).</p>
                      <p>This project maps satellite-tracked pelagic shark and fishing vessel distributions
                        globally to identify space use ‘hotspots’ and quantifies overlap between sharks and
                        fishing effort. This approach provides a fisheries-independent determination of shark
                        spatio-temporal distributions. Modelling is used to explore how changes in ocean
                        environmental variables, such as sea surface temperature (SST), gradients in SST (fronts)
                        and thermocline depths, may drive shark movements and fishing effort distributions. The
                        study estimates exposure risk of large pelagic shark species to longline fisheries
                        globally for informing conservation and management measures. We highlight the huge
                        potential of near-real time satellite surveillance of sharks and fishing vessels for High
                        Seas management.</p>
                    </div>
                  </div>
                  <div class="expand" data-method="expandBio"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="research">
            <div class="row ">
              <div class="col-xs-12 col-lg-8 research-image reveal blur-fade-down" data-delay="200">
                <div class="profile-image">
                  <img src="img/Oceanic-deoxygenation-780x520.jpg" alt=""
                    class="obj-fit-cover blend-img"></div>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="col-xs-12 col-lg-8 research-data-wrapper reveal blur-fade-up align-self-end">
                <div class="person-data">
                  <div class="number">03</div>
                  <h2>Oceanic deoxygenation impacts on threatened sharks</h2>
                  <div class="bio">
                    <p>Dissolved oxygen (DO) has a major role in shaping marine ecosystems by determining the
                      spatio-temporal distributions of all water-breathing marine organisms, from bacteria to
                      fish.</p>
                  </div>
                  <div class="row">
                    <div class="hidden-content col-xs-12 col-lg-12">
                      <p>
                        However global climate change has led to declines in DO in the ocean interior (ocean
                        deoxygenation) that are predicted to continue. Therefore the effects on marine biota are
                        likely to be profound, particularly in oxygen minimum zones (OMZ) where long-term DO
                        declines are acute.</p>
                      <p>OMZs across the world’s oceans are increasing in area and volume as a result of oceanic
                        deoxygenation, with unknown consequences for ecosystems and biodiversity. Potential
                        impacts may be particularly significant for large, fast-swimming oceanic predators with
                        high oxygen demands, such as oceanic pelagic sharks, by reducing available habitats and
                        concentrating them further in surface waters where they are more susceptible to fisheries.
                      </p>
                      <p>Key questions in this study concern the degree by which shark habitats will contract with
                        expanding OMZs, and how this will interact with fisheries to change their distributions.
                        The project has developed and deployed new tag technologies to understand from direct
                        measurements how oceanic sharks actually respond to hypoxic regions. Using the GSMP
                        Database and new data, modelling studies explore the effects of future deoxygenation and
                        OMZ shoaling on shark niches and determine how these may shift distributions and alter
                        their vulnerability to fishing exploitation.</p>
                    </div>
                  </div>
                  <div class="expand" data-method="expandBio"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="research">
            <div class="row justify-content-center">
              <div class="col-xs-12 col-lg-8 research-image reveal blur-fade-down" data-delay="200">
                <div class="profile-image">
                  <img
                    src="img/Climate-change-effects-on-shark-environmental-niches-780x520.jpg"
                    alt="" class="obj-fit-cover blend-img"></div>
              </div>
            </div>
            <div class="row justify-content-end">
              <div class="col-xs-12 col-lg-8 research-data-wrapper reveal blur-fade-up align-self-end">
                <div class="person-data">
                  <div class="number">04</div>
                  <h2>Climate change effects on shark environmental niches</h2>
                  <div class="bio">
                    <p>Shifts in the distributions and abundance of marine organisms are occurring globally as a
                      major consequence of climate change.</p>
                  </div>
                  <div class="row">
                    <div class="hidden-content col-xs-12 col-lg-12">
                      <p>Marine fish are no exception, with significant changes documented across taxa. However,
                        the effects of ocean warming on the niches of oceanic pelagic sharks are poorly understood
                        by comparison. For example, in the North Atlantic Ocean, there are currently no
                        predictions available about how pelagic sharks may respond to future ocean environmental
                        change. Additionally, overfishing of many pelagic shark species is already occurring with
                        significant reductions in catch rates documented for many species.</p>
                      <p>This study addresses an urgent need to understand how ocean climate changes may alter
                        distributions and centres of abundance, which may interact with fisheries to further
                        impact future sustainability of populations. The GSMP database of the movements of
                        satellite-tracked pelagic sharks with 3-D environmental variables extracted per location
                        are used to model present day distributions and environmental niches. By applying outputs
                        from Earth System Models predicting future conditions, we explore how shark distributions
                        may shift over the next century.</p>
                    </div>
                  </div>
                  <div class="expand" data-method="expandBio"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="research">
            <div class="row ">
              <div class="col-xs-12 col-lg-8 research-image reveal blur-fade-down" data-delay="200">
                <div class="profile-image">
                  <img src="img/image8-780x520.jpg" alt=""
                    class="obj-fit-cover blend-img"></div>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="col-xs-12 col-lg-8 research-data-wrapper reveal blur-fade-up align-self-end">
                <div class="person-data">
                  <div class="number">05</div>
                  <h2>Global pelagic shark use of marine protected areas</h2>
                  <div class="bio">
                    <p>How spatially protected might pelagic sharks be given the 14,000+ marine protected areas
                      (MPAs) that exist in coastal, shelf and oceanic regions?</p>
                    <p>This study quantifies the spatial and temporal scales of space use of pelagic sharks in
                      marine protected areas listed by the IUCN World MPA Database and models the levels of
                      spatial protection for sharks if MPAs on the High Seas are increased in area. The outputs
                      allow estimates of the degree of protection for sharks in MPAs with different designations
                      (controls on fishing).</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="research">
            <div class="row justify-content-center">
              <div class="col-xs-12 col-lg-8 research-image reveal blur-fade-down" data-delay="200">
                <div class="profile-image">
                  <img src="img/whale-shark-scarred_Ningaloo2019-780x520.jpg"
                    alt="" class="obj-fit-cover blend-img"></div>
              </div>
            </div>
            <div class="row justify-content-end">
              <div class="col-xs-12 col-lg-8 research-data-wrapper reveal blur-fade-up align-self-end">
                <div class="person-data">
                  <div class="number">06</div>
                  <h2>Risks of whale shark interactions with global shipping routes</h2>
                  <div class="bio">
                    <p>The World’s largest fish, the whale shark, is relatively slow moving and spends significant
                      periods of time near the ocean surface where it is potentially at risk from ship strike.
                      However, the levels of risk posed by increasing global shipping has not been determined
                      across ocean regions.</p>
                    <p>This GSMP study determines the horizontal and vertical movements of whale sharks and their
                      interactions with global AIS shipping movements in space and time. This presents a unique
                      opportunity to quantify overlap and exposure risks of whale sharks to shipping and potential
                      collisions across ocean regions.</p>
                    <p>GSMP collaborating scientists have tracked over 330 individual whale sharks globally to
                      enable this study.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>