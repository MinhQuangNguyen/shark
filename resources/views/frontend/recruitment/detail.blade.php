@extends('frontend.layouts.khangchi')

@section('title', __('Recruitment'))

@section('content')

<div class="banner-recruitment d-block d-lg-none">
    <div class="container-fluid">
        <div class="row">
            <div class="col-14 col-lg-7">
                <div class="background-img"></div>
            </div>
                <div class="col-14 col-lg-7">
                <div class="background-logo">
                    <img src="{{asset('img/logoOpacyti.svg')}}"/>
                    <div class="title">{{ $recruitment->title }}</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="banner-recruitment d-none d-lg-block">
    <div class="container-fluid">
        <div class="row no-gutters">
             <div class="col-14 col-lg-7">
               <div class="background-logo">
                    <img src="{{asset('img/logoOpacyti.svg')}}"/>
                    <div class="title">{{ $recruitment->title }}</div>
                </div>
            </div>
            <div class="col-14 col-lg-7">
                <div class="background-img"></div>
            </div>
        </div>
    </div>
</div>


<div class="item-info-secruitment">
    <div class="background-img"></div>
    <div class="info-main-secruiment-white">
        <div class="container">
            <div class="title-info">@lang('Job Information')</div>
            <div class="row">
                <div class="col-14 col-lg-4">
                    <div class="info-main">
                        <div class="title">@lang('Workplace')</div>
                        <div class="text">{{ $recruitment->address->name }}</div>
                    </div>
                    <div class="info-main">
                        <div class="title">@lang('Rank')</div>
                        <div class="text">{{ $recruitment->rank }}</div>
                    </div>
                    <div class="info-main">
                        <div class="title">@lang('Form')</div>
                        <div class="text">{{ $recruitment->getForm() }}</div>
                    </div>
                    <div class="info-main">
                        <div class="title">@lang('Degree')</div>
                        <div class="text">{{ $recruitment->degree->name }}</div>
                    </div>
                    <div class="info-main">
                        <div class="title">@lang('Wage')</div>
                        <div class="text">{{ number_format($recruitment->salary_start) }}đ – {{ number_format($recruitment->salary_end) }}đ</div>
                    </div>
                    <div class="info-main">
                        <div class="title">@lang('Department')</div>
                        <div class="text">{{ $recruitment->department->name }}</div>
                    </div>
                    <div class="info-main">
                        <div class="title">@lang('Deadline to receive records')</div>
                        <div class="text">{{ date_format($recruitment->expired, 'd/m/Y') }}</div>
                    </div>
                    <div class="info-main">
                        <div class="title">@lang('Location')</div>
                        <div class="text" style="white-space: pre-line;">{!! $recruitment->location !!}</div>
                    </div>
                    <div class="info-main">
                        <div class="title">@lang('Contact')</div>
                        <div class="text" style="white-space: pre-line;">{!! $recruitment->contact !!}</div>
                    </div>
                    <div class="info-main">
                        <div class="title">@lang('Note')</div>
                        <div class="text" style="white-space: pre-line;">{!! $recruitment->note !!}</div>
                    </div>
                </div>
                <div class="col-14 col-lg-10">
                    <div class="info-main-detail">
                    <div class="title-dettail">@lang('Recruitment details')</div>
                    <div class="text" style="white-space: pre-line;">{!! $recruitment->content !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
