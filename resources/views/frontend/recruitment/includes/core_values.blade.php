<div class="banner-customer-recrument">
    @if (isset($core_values))
        <div class="container d-block d-lg-none slide-recrument">
            <div class="row">
                <div class="col-8">
                    <div class="slide-recrument-image-group">
                        <div class="slide-recrument-image-wrap">
                            @foreach ($core_values as $item)
                                <div class="slide-recrument-image-item">
                                    <img src="{{ asset($item->image) }}" alt="" />
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="pagination-main pt-pagination-mqn">
                        <div class="row justify-content-center">
                            <div class="col-14">
                                <div class="btn-kc btn-back partner-prev pb-pagination">
                                    <div class="btn-content"></div>
                                </div>
                            </div>
                            <div class="col-14">
                                <div class="item-number-pagination pb-pagination">
                                    <span class="currentIndex">01</span>
                                    <span>/</span>
                                    <span class="total">03</span>
                                </div>
                            </div>
                            <div class="col-14">
                                <div class="btn-kc btn-next partner-next pb-pagination">
                                    <div class="btn-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-14">
                    <div class="slide-recrument-content">
                        @foreach ($core_values as $item)
                            <div class="slide-recrument-content-item animate__animated animate__fadeOut">
                                <div class="title">{{ $item->title }}</div>
                                {!! $item->content !!}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (isset($core_values))
        <div class="container d-none d-lg-block slide-recrument">
            <div class="row">
                <!-- <div class="col-2 col-xl-2 d-lg-none d-xl-block"></div> -->
                <div class="col-7 col-lg-4 col-xl-3">
                    <div class="slide-recrument-image-group">
                        <div class="slide-recrument-image-wrap">
                            @foreach ($core_values as $item)
                                <div class="slide-recrument-image-item">
                                    <img src="{{ asset($item->image) }}" alt="" />
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-14 col-lg-7 col-xl-9">
                    <div class="slide-recrument-content">
                        @foreach ($core_values as $item)
                            <div class="slide-recrument-content-item animate__animated animate__fadeOut">
                                <div class="title">{{ $item->title }}</div>
                                {!! $item->content !!}
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-4 col-lg-2 col-xl-2">
                    <div class="pagination-main pt-pagination-mqn">
                        <div class="row justify-content-center">
                            <div class="col-14">
                                <div class="btn-kc btn-back partner-prev pb-pagination">
                                    <div class="btn-content"></div>
                                </div>
                            </div>
                            <div class="col-14">
                                <div class="item-number-pagination pb-pagination">
                                    <span class="currentIndex">01</span>
                                    <span>/</span>
                                    <span class="total">03</span>
                                </div>
                            </div>
                            <div class="col-14">
                                <div class="btn-kc btn-next partner-next pb-pagination">
                                    <div class="btn-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
