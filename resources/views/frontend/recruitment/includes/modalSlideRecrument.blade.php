<div class="modal fade {{ $modalName }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- header slide -->
            <div class="home-slide slide-kc modalSlide-main">
                <div id="modal-slide-utilities" class="carousel slide  w-95" data-ride="carousel" data->
                    <div class="carousel-inner">
                        @foreach ($images as $key => $image)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <div class="img" style="background-image: url({{ asset($image['image']) }})"></div>
                                {{-- @if ($captions[$key])
                                    <div class="carousel-caption">
                                        <div class="item-slide-project">
                                            <div class="title">{{ $captions[$key] }}</div>
                                        </div>
                                    </div>
                                @endif --}}
                            </div>
                        @endforeach

                    </div>
                    <a class="carousel-control-prev" href=".{{ $modalName }} .carousel" role="button" data-slide="prev">
                        <div class="btn-kc btn-back">
                            <div class="btn-content"></div>
                        </div>
                    </a>
                    <a class="carousel-control-next" href=".{{ $modalName }} .carousel" role="button" data-slide="next">
                        <div class="btn-kc btn-next">
                            <div class="btn-content"></div>
                        </div>
                    </a>
                </div>

            </div>
            <!-- end header slide -->

        </div>
    </div>
</div>
