<div class="searchNews">
    <form class="searchForm">
        <div class="container">
            <div class="row">
                <div class="col-14 col-lg-14">
                    <h4 class="title pb-mqn">@lang('Recruitment')</h4>
                </div>
            </div>
            <div class="row justify-content-around justify-content-lg-between">
                <div class="col-14 col-lg-6">
                    <div class="search-news">
                        <input type="text" class="white-input search-inp" placeholder="Tìm kiếm…" name="keyword" value="{{ request()->get('keyword') }}">
                        <div class="search-icon"><i class="fas fa-search"></i></div>
                    </div>
                </div>
                <div class="col-14 col-lg-1"></div>
                <div class="col-14 col-lg-2">
                    <select name="career_id" class="selectpicker white-select search-select recruitment" data-style="red-select-btn">
                        <option value="" style="display:none">@lang('Career')</option>
                        @foreach($careers as $item)
                            <option value="{{ $item->id }}" {{ request()->get('career_id') == $item->id ? 'selected' : ''}}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-14 col-lg-2">
                    <select name="address_id" class="selectpicker white-select search-select recruitment" data-style="red-select-btn">
                        <option value="" style="display:none">@lang('Address')</option>
                        @foreach($addresses as $item)
                            <option value="{{ $item->id }}" {{ request()->get('address_id') == $item->id ? 'selected' : ''}}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-14 col-lg-2">
                    <select name="wage" class="selectpicker white-select search-select recruitment" data-style="red-select-btn">
                        <option value="" style="display:none">@lang('Wage')</option>
                        @foreach($wages as $item)
                        <option value="{{ $item['id'] }}" {{ request()->get('wage') == $item['id'] ? 'selected' : ''}}>{{ $item['label'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
