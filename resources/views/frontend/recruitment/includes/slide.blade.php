  <!-- header slide -->
  <div class="home-slide slide-kc slide-recuitment">
    <div id="home-slide-main" class="carousel slide w-95" data-ride="carousel">
        <div class="carousel-inner">
            @if(isset($slides))
            @foreach($slides as $key => $slide)
            <div class="carousel-item {{ $key === 0 ? 'active' : ''}}">
                <div class="img" style="background-image: url({{ asset($slide->image) }})"></div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
    <div class="title-group">
        @if(isset($slides))
        @foreach($slides as $key => $slide)
        <div class="item padding-none active">
            <div class="item-slide-recuitment">
                <div class="title">{{ $slide->title }}</div>
            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>
<!-- end header slide -->
