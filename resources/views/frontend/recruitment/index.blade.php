@extends('frontend.layouts.khangchi')

@section('title', __('Recruitment'))

@section('content')

    @include('frontend.recruitment.includes.slide', $slides)

    <?php $lang = app()->getLocale() === 'vi' ? '/tuyen-dung' : '/recruitment'; ?>
    <div class="item-info-secruitment">
        <div class="background-img"></div>
        <div class="info-main-secruiment-white pb-none">
            <div class="container">
                <div class="title-info">@lang('Core values')</div>
                @include('frontend.recruitment.includes.about')
                @include('frontend.recruitment.includes.core_values', $core_values)
            </div>
        </div>

        <div class="about-recrument slide-show-modal" data-target=".modalSlideRecrument">
            <div class="container">
                <div class="title">@lang('Working environment in Khang Chi')</div>
                <div class="row no-gutters">
                    <div class="col-14 col-lg-10">
                        <div class="row no-gutters">
                            @if (isset($images))
                                <div class="col-14 col-lg-9">
                                    <div class="item-mqn-main-background-about item" data-toggle="modal"
                                        data-target=".modalSlideRecrument" data-key="0">
                                        <div class="item-background-abour-recruitment-mqn">
                                            <div class="background-image-about-recrument pic-1"
                                                style="background-image: url('{{ isset($images[0]) ? asset($images[0]['image']) : '' }}');">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-14 col-lg-5">
                                    <div class="item-mqn-main-background-about item" data-toggle="modal"
                                    data-target=".modalSlideRecrument" data-key="1">
                                        <div class="item-background-abour-recruitment-mqn">
                                            <div class="background-image-about-recrument pic-2"
                                                style="background-image: url('{{ isset($images[1]) ? asset($images[1]['image']) : '' }}');">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-14 col-lg-5">
                                    <div class="item-mqn-main-background-about item" data-toggle="modal"
                                    data-target=".modalSlideRecrument" data-key="2">
                                        <div class="item-background-abour-recruitment-mqn">
                                            <div class="background-image-about-recrument pic-2"
                                                style="background-image: url('{{ isset($images[2]) ? asset($images[2]['image']) : '' }}');">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-14 col-lg-9">
                                    <div class="item-mqn-main-background-about item"  data-toggle="modal"
                                    data-target=".modalSlideRecrument" data-key="3">
                                        <div class="item-background-abour-recruitment-mqn">
                                            <div class="background-image-about-recrument  pic-1"
                                                style="background-image: url('{{ isset($images[2]) ? asset($images[3]['image']) : '' }}');">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-14 col-lg-4">
                        <div class="item-background-pic3 item"  data-toggle="modal"
                        data-target=".modalSlideRecrument" data-key="4">
                            @if (isset($images))
                                <div class="background-image-about-recrument pc-img-3"
                                    style="background-image: url('{{ isset($images[2]) ? asset($images[3]['image']) : '' }}');">
                                </div>
                                <div class="text-info">
                                    <div class="title">+{{ count($images) - 4 }}</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('frontend.recruitment.includes.modalSlideRecrument', [
            'modalName' => 'modalSlideRecrument',
            'images' => $images,
            'captions' => []
        ])

        <div class="search-part-main">
            <div class="background-white-mqn padding-top-background-mqn"></div>
            <div class="container">
                <div class="row">
                    <div class="col-14 col-lg-5">
                        <div class="title-search">@lang('Looking for opportunities in Khang Chi')</div>
                    </div>
                    <div class="col-14 col-lg-9">
                        @if (isset($recuitments))
                            @foreach ($recuitments as $key => $recuitment)
                                <a href="{{ $lang . '/' . $recuitment->slug . '.html' }}">
                                    <div class="item-info-list-recruitment">
                                        <div class="title">{{ $recuitment->title }}</div>
                                        <div class="text" style="white-space: pre-line;">{{ $recuitment->description }}
                                        </div>
                                        <div class="info-more-list-recruiment">
                                            <div class="mobile d-block d-lg-none">
                                                <div class="row">
                                                    <div class="col-8 col-lg-7">
                                                        <div class="text">{{ date_format($recuitment->expired, 'd/m/Y') }}
                                                        </div>
                                                        <div class="text">{{ number_format($recuitment->salary_start) }}đ –
                                                            {{ number_format($recuitment->salary_end) }}đ
                                                        </div>
                                                    </div>
                                                    <div class="col-6 col-lg-7">
                                                        <div class="text-location">{{ $recuitment->address->name }}</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="deskop d-none d-lg-block">
                                                <div class="row">
                                                    <div class="col-14 col-lg-3">
                                                        <div class="text">{{ date_format($recuitment->expired, 'd/m/Y') }}
                                                        </div>
                                                    </div>
                                                    <div class="col-14 col-lg-3">
                                                        <div class="text">{{ $recuitment->address->name }}</div>
                                                    </div>
                                                    <div class="col-14 col-lg-5">
                                                        <div class="text">{{ number_format($recuitment->salary_start) }}đ –
                                                            {{ number_format($recuitment->salary_end) }}đ
                                                        </div>
                                                    </div>
                                                    <div class="col-14 col-lg-3"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        @endif

                        <div class="readmore-red readmore-project pt-more">
                            <a href="{{ $lang . '/list' }}">@lang('View all')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
