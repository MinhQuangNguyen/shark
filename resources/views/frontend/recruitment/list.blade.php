@extends('frontend.layouts.khangchi')

@section('title', __('Recruitment'))

@section('content')
<?php $lang = app()->getLocale() === 'vi' ? '/tuyen-dung' : '/recruitment'; ?>

<div class="banner-recruitment d-block d-lg-none">
        <div class="container-fluid">
            <div class="row">
                <div class="col-14 col-lg-7">
                    <div class="background-img"></div>
                </div>
                 <div class="col-14 col-lg-7">
                   <div class="background-logo">
                        <img src="{{asset('img/logoOpacyti.svg')}}"/>
                        <div class="title">@lang('Building a professional and dynamic working environment')</div>
                    </div>
                </div>
            </div>
        </div>
</div>

<div class="banner-recruitment d-none d-lg-block">
    <div class="container-fluid">
        <div class="row no-gutters">
             <div class="col-14 col-lg-7">
               <div class="background-logo">
                    <img src="{{asset('img/logoOpacyti.svg')}}"/>
                    <div class="title">@lang('Building a professional and dynamic working environment')</div>
                </div>
            </div>
            <div class="col-14 col-lg-7">
                <div class="background-img"></div>
            </div>
        </div>
    </div>
</div>

<div class="item-info-secruitment">
    <div class="background-img"></div>
    <div class="info-main-secruiment-white">
        @include('frontend.recruitment.includes.search')
        <div class="container">
            <div class="item-list-recruitment">
                <div class="row">
                    @if(isset($recuitments))
                    @foreach($recuitments as $key => $recuitment)
                    <div class="col-14 col-lg-7">
                        <a href="{{ $lang.'/'.$recuitment->slug.'.html' }}">
                            <div class="item-info-list-recruitment">
                                <div class="title">{{ $recuitment->title }}</div>
                                <div class="text" style="white-space: pre-line;">{{ $recuitment->description }}</div>
                                <div class="info-more-list-recruiment">
                                    <div class="mobile d-block d-lg-none">
                                        <div class="row">
                                            <div class="col-8 col-lg-7">
                                                <div class="text">{{ $recuitment->expired ? date_format($recuitment->expired, 'd/m/Y') : null }}</div>
                                                <div class="text">{{ number_format($recuitment->salary_start) }}đ – {{ number_format($recuitment->salary_end) }}đ</div>
                                            </div>
                                            <div class="col-6 col-lg-7">
                                                <div class="text-location">{{ $recuitment->address->name }}</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="deskop d-none d-lg-block">
                                        <div class="row">
                                            <div class="col-14 col-lg-3">
                                                <div class="text">{{ $recuitment->expired ? date_format($recuitment->expired, 'd/m/Y') : null }}</div>
                                            </div>
                                            <div class="col-14 col-lg-3">
                                                <div class="text">{{ $recuitment->address->name }}</div>
                                            </div>
                                            <div class="col-14 col-lg-6">
                                                <div class="text">{{ number_format($recuitment->salary_start) }}đ – {{ number_format($recuitment->salary_end) }}đ</div>
                                            </div>
                                            <div class="col-14 col-lg-3"></div>
                                        </div>
                                    </div>
                                 </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    @endif
                </div>

                {{ $recuitments->appends(request()->query())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
