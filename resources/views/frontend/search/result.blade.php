@extends('frontend.layouts.khangchi')

@section('title', __('Search'))

@section('content')
<?php $lang = app()->getLocale() === 'vi' ? '/tim-kiem' : '/search'; ?>
<div class="banner-recruitment d-block d-lg-none">
    <div class="container-fluid">
        <div class="row">
            <div class="col-14 col-lg-7">
                <div class="background-img"></div>
            </div>
            <div class="col-14 col-lg-7">
            <div class="background-logo">
                    <img src="{{asset('img/logoOpacyti.svg')}}"/>
                    <div class="title">@lang('Search Results')</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="banner-recruitment d-none d-lg-block">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-14 col-lg-7">
            <div class="background-logo">
                    <img src="{{asset('img/logoOpacyti.svg')}}"/>
                    <div class="title">@lang('Search Results')</div>
                </div>
            </div>
            <div class="col-14 col-lg-7">
                <div class="background-img"></div>
            </div>
        </div>
    </div>
</div>


<div class="item-info-secruitment">
    <div class="background-img"></div>
    <div class="info-main-secruiment-white">
        <div class="container">
            <div class="title-info">@lang('Result')</div>
            <div class="desc">{{ __('Result :begin - :end of :total', ['begin' => $results->firstItem(), 'end' => $results->lastItem(), 'total' => $results->total() ]) }}</div>
            <div class="row">
                @if(isset($results))
                @foreach($results as $key => $result)
                <div class="col-14 col-lg-7">
                    <div class="info-main-result">
                        <div class="title"><a href="{{ $result->category->getLink().'/'.$result->category->slug.'/'.$result->slug.'.html' }}">{{ $result->name }}</a></div>
                        <div class="text">{{ $result->description }}</div>
                        <div class="text">
                            <a href="{{ $result->category->getLink() }}">{{ $result->category->convertType() }}</a> | <a href="{{ $result->category->getLink().'/'.$result->category->slug }}">{{ $result->category->name }}</a>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>

            {{ $results->withPath($lang)->appends(request()->query())->links() }}
        </div>
    </div>
</div>
@endsection
